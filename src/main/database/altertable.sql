

alter table lesson add column start_date DATE null;
alter table lesson add column end_date DATE null;
alter table lesson add column start_hour TIME null;
alter table lesson add column end_hour TIME null;
alter table lesson add column schedule varchar(255) null;

alter table lesson drop column schedule;