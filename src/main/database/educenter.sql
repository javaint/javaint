
create table users (
    username varchar(50) not null primary key,
    password varchar(120) not null,
    fname varchar(120) null,
    lname varchar(120) null,
    lname integer(8) null,
    email varchar(120) null,
    enabled boolean not null
);

create table authorities (
	id integer unsigned auto_increment primary key,	
    username varchar(50) not null,
    authority varchar(50) not null,
    foreign key (username) references users (username) on update cascade
);

create table category (
	id integer unsigned auto_increment not null,
	name varchar(255) not null,	
	status varchar(255) not null,
	ordering integer not null,
	counter integer null,
	primary key(id)
);

create table teacher (
	id integer unsigned auto_increment not null,
	fname varchar(255) not null,
	lname varchar(255) not null,
	register varchar(10) not null,
	profession varchar(255) not null,
	email varchar(255) null,
	phone varchar(255) not null,
	image_id integer unsigned,
	foreign key (image_id) references image(id),
	primary key(id)
);

create table student (
	id integer unsigned auto_increment not null,
	fname varchar(255) not null,
	lname varchar(255) not null,
	register varchar(10) not null,
	phone integer not null,
	address varchar(255) null,
	email varchar(255) null,
	profession varchar(255) not null,
	primary key(id)
);
	

create table lesson (
	id integer unsigned auto_increment not null,
	lesson_name varchar(255) not null,
	category_id integer unsigned not null,
	teacher_id integer unsigned not null,
	image_id integer unsigned,
	description varchar(255) null,
	text TEXT null,
	primary key(id),
	foreign key (image_id) references image(id),
	FOREIGN KEY (category_id) REFERENCES category(id),
	FOREIGN KEY (teacher_id) REFERENCES teacher(id)
);


create table selection ( 
	id integer(50) unsigned not null auto_increment, 
	student_id integer(50) unsigned not null, 
	foreign key(student_id) references student(id) on delete no action, 
	lselect_id integer(50) unsigned not null, 
	foreign key(lselect_id) references lessonselect(id) on delete no action,
	primary key(id)
);

CREATE TABLE lessonselect(
	 id INT unsigned auto_increment  NOT NULL,
	 lesson_id INT unsigned NOT NULL,
	 weekday varchar(120) null,
	 start_date DATE null,
	 end_date DATE null,
	 start_hour varchar(255) null,
	 end_hour varchar(255) null,
	 FOREIGN KEY (lesson_id) references lesson(id) on delete no action, 
	 PRIMARY KEY (id)
);
CREATE TABLE newClass(
	 id INT unsigned auto_increment  NOT NULL,
	 lesson_id INT unsigned NOT NULL,
	 weekday varchar(120) null,
	 start_date DATE null,
	 end_date DATE null,
	 start_hour varchar(255) null,
	 end_hour varchar(255) null,
	 FOREIGN KEY (lesson_id) references lesson(id) on delete no action, 
	 PRIMARY KEY (id)
);

CREATE TABLE lessonselect_item(
	 id integer unsigned auto_increment not null,
	 student_id integer unsigned not null,
	 lessonselect_id integer unsigned not null,
	 FOREIGN KEY (student_id) references student(id) on delete no action, 
	 FOREIGN KEY (lessonselect_id) references lessonselect(id) on delete no action, 
	 PRIMARY KEY (id)
);