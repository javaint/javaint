package mn.educenter.app.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import mn.educenter.app.domain.Category;
import mn.educenter.app.domain.Lesson;
import mn.educenter.app.repository.CategoryRepository;
import mn.educenter.app.repository.LessonRepository;


@Controller
public class LessonWebController {

	@Autowired
	private LessonRepository lessonRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@GetMapping("list")
	public String list (Model model, @RequestParam(defaultValue = "0") Integer page) {
		PageRequest request = new PageRequest(page, 5, Sort.Direction.DESC, "id");
		Page<Lesson> lessons = lessonRepository.findAll(request);	
		model.addAttribute("lessons", lessons);
		return "lesson/list";		
	}
	
	@GetMapping("category/{category_id}")
	public String category (Model model, @PathVariable Integer category_id, @RequestParam(defaultValue = "0") Integer page) {
		Category category = categoryRepository.findOne(category_id);
		model.addAttribute("category", category);
		Page<Lesson> lessons = lessonRepository.findByCategory(category_id, new PageRequest(page, 5, Sort.Direction.DESC, "id"));	
		model.addAttribute("lessons", lessons);
		return "lesson/catlist";		
	}
	
	@GetMapping("{id}")
	public String read (Model model, @PathVariable Integer id) {
		Lesson lesson = lessonRepository.findOne(id);	
		model.addAttribute("lesson", lesson);
		return "lesson/one";		
	}
		
	
}
