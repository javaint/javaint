package mn.educenter.app.controller.admin;

import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.educenter.app.domain.Selection;
import mn.educenter.app.domain.Student;
import mn.educenter.app.repository.LessonRepository;
import mn.educenter.app.repository.LessonselectRepository;
import mn.educenter.app.repository.SelectionRepository;
import mn.educenter.app.repository.StudentRepository;


@Controller
@RequestMapping(value = "/admin/selection")

public class SelectionController {
	@Autowired
	private SelectionRepository Srepo;
	
	@Autowired
	private LessonRepository lessonRepo;
	
	@Autowired
	private LessonselectRepository lselectRepo;
	
	@Autowired
	private StudentRepository studentRepo;

	@GetMapping
	public String main(@RequestParam Integer lselect_id, Map<String, Object> model) {
		model.put("selection", lselectRepo.findOne(lselect_id));
		return "admin/selection";
	}

	@GetMapping("list")
	public String list(@RequestParam Integer lselect_id, Model model) {
		List<Selection> selection = Srepo.findByLselectId(lselect_id);
		model.addAttribute("selections", selection);
		return "admin/selection/list"; 
	}

	@GetMapping("save")
	@ResponseBody
	public String save(@RequestParam Integer student_id, @RequestParam Integer lselect_id) {
		Selection selection = new Selection();
		selection.setLselect_id(lselect_id);
		selection.setStudent(studentRepo.findOne(student_id));

			
		if (Srepo.find(student_id, lselect_id).isEmpty()) {
			Srepo.save(selection);
			
			Student student = studentRepo.findOne(student_id);
			studentRepo.save(student);
			
			return "success";
		} else {
			return "duplicate";
		}		
	}

	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		Selection selection = Srepo.findOne(id);
		Srepo.delete(selection);
	}

	
}
