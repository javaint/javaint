package mn.educenter.app.controller.admin;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

import mn.educenter.app.business.FileService;
import mn.educenter.app.domain.Image;
import mn.educenter.app.repository.ImageRepository;



@Controller
@RequestMapping(value="/admin/image")
public class ImageAdminController {
	
	@Autowired
	private FileService service;
	
	@Autowired
	private ImageRepository repo;
	
	@GetMapping
	public String main () {		
		return "admin/image";		
	}
				
	@PostMapping("upload")
	public String uploadFile(@RequestParam MultipartFile file) throws Exception{
		if (service.isImageFile(file)) {
			Image image = service.uploadImage(file);
			image.setCreated(new Date());		
			repo.save(image);			
		}
		return "redirect:/admin/image";
	}
		
	@GetMapping("list")
	public String list (Model model, Pageable pageable) {		
		model.addAttribute("images", repo.findAll(pageable));
		return "admin/image/list";		
	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		Image image = repo.findOne(id);
		repo.delete(image);

	}
	
	@GetMapping("select")
	public String select (Model model, Pageable pageable) {		
		model.addAttribute("images", repo.findAll(pageable));
		return "admin/image/select";		
	}
	
	@GetMapping("select/{id}")
	public String getSelected (Model model, @PathVariable Integer id) {		
		model.addAttribute("image", repo.findOne(id));
		return "admin/image/selectedImage";		
	}
	
}
