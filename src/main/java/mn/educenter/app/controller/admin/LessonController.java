package mn.educenter.app.controller.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.educenter.app.domain.Category;
import mn.educenter.app.domain.Lesson;
import mn.educenter.app.domain.Teacher;
import mn.educenter.app.modal.LessonForm;
import mn.educenter.app.repository.CategoryRepository;
import mn.educenter.app.repository.ImageRepository;
import mn.educenter.app.repository.LessonRepository;
import mn.educenter.app.repository.NewClassRepository;
import mn.educenter.app.repository.TeacherRepository;


@Controller
@RequestMapping(value = "/admin/lesson")

public class LessonController {
	@Autowired
	private LessonRepository lessonRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private TeacherRepository teacherRepository;
	
	@Autowired
	private ImageRepository imageRepository;
	
	@Autowired
	private NewClassRepository newclassRepository;

	@GetMapping
	public String main() {
		return "admin/lesson";
	}

	@GetMapping("list")
	public String list(Model model, @RequestParam(required = false) String name,@RequestParam(required = false) Integer category_id, Pageable pageable) {
		
		if (category_id == null) {
			model.addAttribute("lesson", lessonRepository.findByName(name == null ? "" : name, pageable));			
		}
		else {
			System.out.println(category_id);
			model.addAttribute("lesson", lessonRepository.findByCategory(category_id, pageable));
		}	
		return "admin/lesson/list";
	}
	
	@GetMapping("list2selection")
	public String list2selection (Model model, @RequestParam(required = false) String name, @RequestParam Integer category_id) {		
		model.addAttribute("lessons", lessonRepository.findByCategory(category_id, (name == null ? "" : name)));
		return "admin/lesson/list2selection";		
	}


	@GetMapping("new")
	public String newForm(Map<String, Object> model) {
		model.put("jspform", new LessonForm());
		model.put("categories", categoryRepository.findAll());
		model.put("teacher", teacherRepository.findAll());
		return "admin/lesson/new";
	}
	
	@GetMapping("classlist")
	public String list (Model model, @RequestParam(required = false) Integer id, Pageable pageable) {
		model.addAttribute("newclass", newclassRepository.findByClass(id, pageable)); 
		return "admin/lesson/classlist";		
	}

	@PostMapping("saveNew")
	public String saveNew(@Valid @ModelAttribute("jspform")LessonForm form, BindingResult bindingResult, Model model) throws ParseException {
		
		model.addAttribute("categories", categoryRepository.findAll());
		model.addAttribute("teacher", teacherRepository.findAll());
			
		if (bindingResult.hasErrors()) {
			model.addAttribute("jspform", form);
			model.addAttribute("lessons", lessonRepository.findAll());
			return "admin/lesson/new";
        }
		
		Integer oldCategoryId = 0;
		Integer oldTeacherId = 0;
		
		Lesson lesson;	
		lesson = new Lesson();
		lesson.setName(form.getName());
		lesson.setWeekday(form.getWeekday());
		lesson.setStart_date(new SimpleDateFormat("yyyy-MM-dd").parse(form.getStart_date()));
		lesson.setEnd_date(new SimpleDateFormat("yyyy-MM-dd").parse(form.getEnd_date()));
		lesson.setStart_hour(form.getStart_hour());
		lesson.setEnd_hour(form.getEnd_hour());
		lesson.setDescription(form.getDescription());
		lesson.setText(form.getText());
		lesson.setCategory(form.getCategory_id() == null ? null : categoryRepository.findOne(form.getCategory_id()));
		lesson.setTeacher(form.getTeacher_id() == null ? null : teacherRepository.findOne(form.getTeacher_id()));
		lesson.setImage(form.getImageId() == null ? null : imageRepository.findOne(form.getImageId()));
		
		lessonRepository.save(lesson);
		
		if (oldCategoryId!=form.getCategory_id()) {
			
			if (oldCategoryId!=0) {
				Integer count =  lessonRepository.countByCategory(oldCategoryId);
				Category category = categoryRepository.findOne(oldCategoryId);
				category.setCounter(count);
				categoryRepository.save(category);
			}
						
			Integer count =  lessonRepository.countByCategory(form.getCategory_id());
			Category category = categoryRepository.findOne(form.getCategory_id());
			category.setCounter(count);
			categoryRepository.save(category);
			
		}
		
		if (oldTeacherId!=form.getTeacher_id()) {
					
				if (oldTeacherId!=0) {
					Integer count =  lessonRepository.countByTeacher(oldTeacherId);
					Teacher teacher = teacherRepository.findOne(oldTeacherId);
					teacher.setCounter(count);
					teacherRepository.save(teacher);
				}
							
				Integer count =  lessonRepository.countByTeacher(form.getTeacher_id());
				Teacher teacher = teacherRepository.findOne(form.getTeacher_id());
				teacher.setCounter(count);
				teacherRepository.save(teacher);
				
			}
		
		
				
		return "redirect:/admin/lesson";
		
		
		/*if (lessonRepository.findByWeekday(form.getWeekday()).isEmpty()) {
			lessonRepository.save(lesson);
			return "redirect:/admin/lesson";
		} else {
			bindingResult.addError(new FieldError("jspform", "weekday", "Гараг давхацсан байна"));
		}
		return "admin/lesson/edit";*/
		
		/*if (lessonRepository.findByStarthour(form.getStart_hour()).isEmpty()) {
			lessonRepository.save(lesson);
			return "redirect:/admin/lesson";
		} else {
			bindingResult.addError(new FieldError("jspform", "start_hour", "Хуваарь давхацсан байна"));
		}

		return "admin/lesson/edit";*/
		
		/*if (lessonRepository.findByCategoryId(form.getCategory_id()).isEmpty()) {
			lessonRepository.save(lesson);
			return "redirect:/admin/lesson";
		} else {
			bindingResult.addError(new FieldError("jspform", "category_id", "Категори давхацсан байна"));
		}
		return "admin/lesson/edit";*/
	}
	
	@PostMapping("saveEdit")
	public String saveEdit(@Valid @ModelAttribute("jspform")LessonForm form, BindingResult bindingResult, Model model) throws ParseException {
		
		model.addAttribute("categories", categoryRepository.findAll());
		model.addAttribute("teacher", teacherRepository.findAll());
			
		if (bindingResult.hasErrors()) {
			model.addAttribute("jspform", form);
			model.addAttribute("lessons", lessonRepository.findAll());
			return "admin/lesson/edit";
        }
		
		Integer oldCategoryId = 0;
		Integer oldTeacherId = 0;
		
		Lesson lesson;	
		lesson = lessonRepository.findOne(form.getId());
		oldCategoryId = lesson.getCategory() == null ? 0 : lesson.getCategory().getId();
		oldTeacherId = lesson.getTeacher() == null ? 0 : lesson.getTeacher().getId();
		lesson.setName(form.getName());
		lesson.setWeekday(form.getWeekday());
		lesson.setStart_date(new SimpleDateFormat("yyyy-MM-dd").parse(form.getStart_date()));
		lesson.setEnd_date(new SimpleDateFormat("yyyy-MM-dd").parse(form.getEnd_date()));
		lesson.setStart_hour(form.getStart_hour());
		lesson.setEnd_hour(form.getEnd_hour());
		lesson.setDescription(form.getDescription());
		lesson.setText(form.getText());
		lesson.setCategory(form.getCategory_id() == null ? null : categoryRepository.findOne(form.getCategory_id()));
		lesson.setTeacher(form.getTeacher_id() == null ? null : teacherRepository.findOne(form.getTeacher_id()));
		lesson.setImage(form.getImageId() == null ? null : imageRepository.findOne(form.getImageId()));
		
		lessonRepository.save(lesson);
		
		if (oldCategoryId!=form.getCategory_id()) {
			
			if (oldCategoryId!=0) {
				Integer count =  lessonRepository.countByCategory(oldCategoryId);
				Category category = categoryRepository.findOne(oldCategoryId);
				category.setCounter(count);
				categoryRepository.save(category);
			}
						
			Integer count =  lessonRepository.countByCategory(form.getCategory_id());
			Category category = categoryRepository.findOne(form.getCategory_id());
			category.setCounter(count);
			categoryRepository.save(category);
			
		}
		
		if (oldTeacherId!=form.getTeacher_id()) {
					
				if (oldTeacherId!=0) {
					Integer count =  lessonRepository.countByTeacher(oldTeacherId);
					Teacher teacher = teacherRepository.findOne(oldTeacherId);
					teacher.setCounter(count);
					teacherRepository.save(teacher);
				}
							
				Integer count =  lessonRepository.countByTeacher(form.getTeacher_id());
				Teacher teacher = teacherRepository.findOne(form.getTeacher_id());
				teacher.setCounter(count);
				teacherRepository.save(teacher);
				
			}
		
		
				
		return "redirect:/admin/lesson";
		
		
		/*if (lessonRepository.findByWeekday(form.getWeekday()).isEmpty()) {
			lessonRepository.save(lesson);
			return "redirect:/admin/lesson";
		} else {
			bindingResult.addError(new FieldError("jspform", "weekday", "Гараг давхацсан байна"));
		}
		return "admin/lesson/edit";*/
		
		/*if (lessonRepository.findByStarthour(form.getStart_hour()).isEmpty()) {
			lessonRepository.save(lesson);
			return "redirect:/admin/lesson";
		} else {
			bindingResult.addError(new FieldError("jspform", "start_hour", "Хуваарь давхацсан байна"));
		}

		return "admin/lesson/edit";*/
		
		/*if (lessonRepository.findByCategoryId(form.getCategory_id()).isEmpty()) {
			lessonRepository.save(lesson);
			return "redirect:/admin/lesson";
		} else {
			bindingResult.addError(new FieldError("jspform", "category_id", "Категори давхацсан байна"));
		}
		return "admin/lesson/edit";*/
	}
	

	@GetMapping("{id}/edit")
	public String edit(@PathVariable Integer id, Map<String, Object> model) {
		Lesson lesson = lessonRepository.findOne(id);
		LessonForm lessonForm = new LessonForm();
		lessonForm.setId(lesson.getId());
		lessonForm.setName(lesson.getName());
		lessonForm.setWeekday(lesson.getWeekday());
		lessonForm.setStart_date(new SimpleDateFormat("yyyy-MM-dd").format(lesson.getStart_date()));
		lessonForm.setEnd_date(new SimpleDateFormat("yyyy-MM-dd").format(lesson.getEnd_date()));
		lessonForm.setStart_hour(lesson.getStart_hour());
		lessonForm.setEnd_hour(lesson.getEnd_hour());
		lessonForm.setDescription(lesson.getDescription());
		lessonForm.setText(lesson.getText());
		lessonForm.setCategory_id(lesson.getCategory() == null ? null : lesson.getCategory().getId());
		lessonForm.setTeacher_id(lesson.getTeacher() == null ? null : lesson.getTeacher().getId());
		lessonForm.setImageId(lesson.getImage() == null ? null : lesson.getImage().getId());
		model.put("categories", categoryRepository.findAll());
		model.put("teacher", teacherRepository.findAll());
		model.put("jspform", lessonForm);
		
		return "admin/lesson/edit";
		
		
	}


	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		Lesson lesson = lessonRepository.findOne(id);
		lessonRepository.delete(lesson);
		
		
		Integer count =  lessonRepository.countByCategory(lesson.getCategory().getId());
		Category category = categoryRepository.findOne(lesson.getCategory().getId());
		category.setCounter(count);
		categoryRepository.save(category);
		
		Integer countt =  lessonRepository.countByTeacher(lesson.getTeacher().getId());
		Teacher teacher = teacherRepository.findOne(lesson.getTeacher().getId());
		teacher.setCounter(countt);
		teacherRepository.save(teacher);

	}
	
	/*@GetMapping("select")
	public String select (Model model, Pageable pageable) {		
		model.addAttribute("lessons", lessonRepository.findAll(pageable));
		return "admin/lesson/select";		
	}
	
	@GetMapping("select/{id}")
	public String getSelected (Model model, @PathVariable Integer id) {		
		model.addAttribute("lesson", lessonRepository.findOne(id));
		return "admin/lesson/selectedLesson";		
	}*/

	
}
