package mn.educenter.app.controller.admin;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.educenter.app.domain.Users;
import mn.educenter.app.modal.UsersForm;
import mn.educenter.app.repository.UsersRepository;

//@Secured("ROLE_ADMIN")
@Controller
@RequestMapping (value ="/admin/users")
public class UsersController {

	@Autowired
	private UsersRepository usersRepository;
	
	
	@GetMapping
	public String main () {		
		return "admin/users";		
	}
	
	@GetMapping("list")
	public String list(Model model, @RequestParam(required = false) String username, Pageable pageable) {
		model.addAttribute("users", usersRepository.findByName(username == null ? "" : username, pageable));
		return "admin/users/list";
	}
	
	@PostMapping("saveEdit")
	public String saveEdit(UsersForm form) {
		
		Users users ;
		users = usersRepository.findOne(form.getUsername());
		users.setUsername(form.getUsername());
		users.setFname(form.getFname());
		users.setLname(form.getLname());
		users.setEmail(form.getEmail());
		users.setPhone(form.getPhone());
		users.setEnabled(form.getEnabled());
		usersRepository.save(users);
		
		return "redirect:/admin/users";
		
	}
	
	@PostMapping("saveNew")
	public String saveNew(UsersForm form) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		Users users = new Users();						
		users.setUsername(form.getUsername());
		users.setFname(form.getFname());
		users.setLname(form.getLname());
		users.setEmail(form.getEmail());
		users.setPhone(form.getPhone());
		users.setPassword(encoder.encode(form.getPassword()));
		users.setEnabled(form.getEnabled());
		usersRepository.save(users);
		
		
		return "redirect:/admin/users";
		
	}
	
	@PostMapping("savePass")
	public String savePass(UsersForm form) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		Users users ;
		users = usersRepository.findOne(form.getUsername());
		users.setUsername(form.getUsername());
		users.setPassword(encoder.encode(form.getPassword()));
		usersRepository.save(users);
		
		return "redirect:/admin/users";
		
	}
	
	@GetMapping("new")
	public String newForm (Model model) {		
		model.addAttribute("jspform", new UsersForm());
		return "admin/users/new";	
	}
	
	@GetMapping("{username}/edit")
	public String edit (@PathVariable String username, Model model) {
		Users users = usersRepository.findOne(username);
		UsersForm form = new UsersForm();		
		form.setUsername(users.getUsername());		
		form.setFname(users.getFname());	
		form.setLname(users.getLname());	
		form.setEmail(users.getEmail());	
		form.setPhone(users.getPhone());	
		form.setEnabled(users.getEnabled());
		model.addAttribute("jspform", form);
		return "admin/users/edit";	
	}
	
	@GetMapping("{username}/editPass")
	public String editPass (@PathVariable String username, Model model) {
		Users users = usersRepository.findOne(username);
		UsersForm form = new UsersForm();		
		form.setUsername(users.getUsername());
		form.setPassword(users.getPassword());
		model.addAttribute("jspform", form);
		return "admin/users/editPass";	
	}
	
	@DeleteMapping("{username}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable String username) {
		Users users = usersRepository.findOne(username);
		usersRepository.delete(users);
		
	}
	
}
