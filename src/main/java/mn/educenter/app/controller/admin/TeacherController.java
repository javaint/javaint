package mn.educenter.app.controller.admin;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.educenter.app.domain.Teacher;
import mn.educenter.app.modal.TeacherForm;
import mn.educenter.app.repository.ImageRepository;
import mn.educenter.app.repository.TeacherRepository;

@Controller
@RequestMapping(value = "/admin/teacher")

public class TeacherController {
	@Autowired
	private TeacherRepository teacherRepository;
	
	@Autowired
	private ImageRepository imageRepository;

	@GetMapping
	public String main() {
		return "admin/teacher";
	}

	@GetMapping("list")
	public String list(Model model, @RequestParam(required = false) String fname, Pageable pageable) {
		model.addAttribute("teacher", teacherRepository.findByName(fname == null ? "" : fname, pageable));
		return "admin/teacher/list";
	}

	@GetMapping("new")
	public String newForm(Map<String, Object> model) {
		model.put("jspform", new TeacherForm());
		return "admin/teacher/new";
	}

	@PostMapping("saveNew")
	public String saveNew(TeacherForm form) {
		Teacher teacher;
		teacher = new Teacher();
		teacher.setFname(form.getFname());
		teacher.setLname(form.getLname());
		teacher.setPhone(form.getPhone());
		teacher.setRegister(form.getRegister());
		teacher.setProfession(form.getProfession());
		teacher.setEmail(form.getEmail());
		teacher.setImage(form.getImageId() == null ? null : imageRepository.findOne(form.getImageId()));
		teacherRepository.save(teacher);
		
		return "redirect:/admin/teacher";
	}
	
	@PostMapping("saveEdit")
	public String saveEdit(TeacherForm form) {
		Teacher teacher;
		teacher = teacherRepository.findOne(form.getId());
		teacher.setFname(form.getFname());
		teacher.setLname(form.getLname());
		teacher.setPhone(form.getPhone());
		teacher.setRegister(form.getRegister());
		teacher.setProfession(form.getProfession());
		teacher.setEmail(form.getEmail());
		teacher.setImage(form.getImageId() == null ? null : imageRepository.findOne(form.getImageId()));
		teacherRepository.save(teacher);
		
		return "redirect:/admin/teacher";
	}


	@GetMapping("{id}/edit")
	public String edit(@PathVariable Integer id, Map<String, Object> model) {
		Teacher teacher = teacherRepository.findOne(id);
		TeacherForm teacherForm = new TeacherForm();
		teacherForm.setId(teacher.getId());
		teacherForm.setFname(teacher.getFname());
		teacherForm.setLname(teacher.getLname());
		teacherForm.setPhone(teacher.getPhone());
		teacherForm.setRegister(teacher.getRegister());
		teacherForm.setProfession(teacher.getProfession());
		teacherForm.setEmail(teacher.getEmail());
		teacherForm.setImageId(teacher.getImage() == null ? null : teacher.getImage().getId());
		model.put("jspform", teacherForm);
		return "admin/teacher/edit";
	}

	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		Teacher teacher = teacherRepository.findOne(id);
		teacherRepository.delete(teacher);

	}

}
