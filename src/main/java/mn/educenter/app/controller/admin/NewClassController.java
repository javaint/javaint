package mn.educenter.app.controller.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.educenter.app.domain.NewClass;
import mn.educenter.app.domain.Selection;
import mn.educenter.app.domain.Student;
import mn.educenter.app.modal.LessonForm;
import mn.educenter.app.modal.NewClassForm;
import mn.educenter.app.repository.LessonRepository;
import mn.educenter.app.repository.NewClassRepository;
import mn.educenter.app.repository.SelectionRepository;
import mn.educenter.app.repository.StudentRepository;


@Controller
@RequestMapping(value = "/admin/newClass")

public class NewClassController {
	
	@Autowired
	private LessonRepository lessonRepo;
	
	@Autowired
	private NewClassRepository newClassRepo;
	

	@GetMapping
	public String main(@RequestParam Integer lesson_id, Map<String, Object> model) {
		model.put("newClass", lessonRepo.findOne(lesson_id));
		return "admin/newClass";
	}
	
	@GetMapping("list")
	public String list(Model model) {
		model.addAttribute("newclass", newClassRepo.findAll());
		return "admin/newClass/list"; 
	}
	
	@GetMapping("new")
	public String newForm (Map<String, Object> model) {
		model.put("jspform", new NewClassForm());
		return "admin/newClass/new";	
	}
	
	@PostMapping("save")
	public String save(@Valid @ModelAttribute("jspform")NewClassForm form, @RequestParam(value = "lesson_id", required =false) Integer lesson_id) throws ParseException {
		
		NewClass newclass ;
		
		newclass= new NewClass();
		
		newclass.setLesson_id(lesson_id);
		newclass.setWeekday(form.getWeekday());
		newclass.setStart_date(new SimpleDateFormat("yyyy-MM-dd").parse(form.getStart_date()));
		newclass.setEnd_date(new SimpleDateFormat("yyyy-MM-dd").parse(form.getEnd_date()));
		newclass.setStart_hour(form.getStart_hour());
		newclass.setEnd_hour(form.getEnd_hour());
		
		newClassRepo.save(newclass);
		
		return "admin/newClass";
		
		
		/*selection.setStudent(studentRepo.findOne(student_id));

			
		if (Srepo.find(student_id, lesson_id).isEmpty()) {
			Srepo.save(selection);
			
			Student student = studentRepo.findOne(student_id);
			studentRepo.save(student);
			
			return "success";
		} else {
			return "duplicate";
		}*/		
	}

	/*

	@GetMapping("save")
	@ResponseBody
	public String save(@RequestParam Integer student_id, @RequestParam Integer lesson_id) {
		NewClass selection = new Selection();
		selection.setLesson_id(lesson_id);
		selection.setStudent(studentRepo.findOne(student_id));

			
		if (Srepo.find(student_id, lesson_id).isEmpty()) {
			Srepo.save(selection);
			
			Student student = studentRepo.findOne(student_id);
			studentRepo.save(student);
			
			return "success";
		} else {
			return "duplicate";
		}		
	}

	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		Selection selection = Srepo.findOne(id);
		Srepo.delete(selection);
	}*/

	
}
