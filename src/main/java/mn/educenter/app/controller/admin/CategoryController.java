package mn.educenter.app.controller.admin;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.educenter.app.domain.Category;
import mn.educenter.app.modal.CategoryForm;
import mn.educenter.app.repository.CategoryRepository;
import mn.educenter.app.repository.LessonRepository;

@Controller
@RequestMapping(value = "/admin/category")
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private LessonRepository lessonRepository;
	

	@GetMapping
	public String main() {
		return "admin/category";
	}
	
	@GetMapping("list")
	public String list(Model model, @RequestParam(required = false) String name, Pageable pageable) {
		model.addAttribute("categories", categoryRepository.findByName(name == null ? "" : name, pageable));
		return "admin/category/list";
	}
	
	

	@GetMapping("new")
	public String newForm(Map<String, Object> model) {
		model.put("jspform", new CategoryForm());
		return "admin/category/new";
	}

	@PostMapping("saveEdit")
	public String saveEdit(@Valid @ModelAttribute("jspform") CategoryForm form, BindingResult bindingResult, Model model) {
		
		if (form.getName()==null) {
			bindingResult.addError(new FieldError("jspform", "name", "custom error"));
		}
		
		if (bindingResult.hasErrors()) {
			model.addAttribute("jspform", form);
			model.addAttribute("category", categoryRepository.findAll());
			return "admin/category/edit";
        }
		
		Category category;
		category = categoryRepository.findOne(form.getId());
		category.setName(form.getName());
		/*
		categoryRepository.save(category);*/
		
		if (categoryRepository.findByCatName(form.getName()).isEmpty()) {
			categoryRepository.save(category);
			return "redirect:/admin/category";
		} else {
			bindingResult.addError(new FieldError("jspform", "name", "Ангилал давхцсан байна"));
		}
		return "admin/category/edit";
	}
	
	@PostMapping("saveNew")
	public String saveNew(@Valid @ModelAttribute("jspform") CategoryForm form, BindingResult bindingResult, Model model) {
		
		if (form.getName()==null) {
			bindingResult.addError(new FieldError("jspform", "name", "custom error"));
		}
		
		if (bindingResult.hasErrors()) {
			model.addAttribute("jspform", form);
			model.addAttribute("category", categoryRepository.findAll());
			return "admin/category/new";
        }
		Category category;
		category = new Category();
		category.setName(form.getName());
		
		
		if (categoryRepository.findByCatName(form.getName()).isEmpty()) {
			categoryRepository.save(category);
			return "redirect:/admin/category";
		} else {
			bindingResult.addError(new FieldError("jspform", "name", "Ангилал давхцсан байна"));
		}
		return "admin/category/new";
}

	@GetMapping("{id}/edit")
	public String edit(@PathVariable Integer id, Map<String, Object> model) {
		Category category = categoryRepository.findOne(id);
		CategoryForm categoryForm = new CategoryForm();
		categoryForm.setId(category.getId());
		categoryForm.setName(category.getName());
		model.put("jspform", categoryForm);
		return "admin/category/edit";
	}

	@DeleteMapping("{id}")
	@ResponseBody
	public String delete(@PathVariable Integer id) {
		Category category = categoryRepository.findOne(id);
		if(category.getCounter() != null) {
			return "Unpossible";
		}else {

			categoryRepository.delete(category);
			return "Ok";
		}

	}
	
	@GetMapping("{id}/count")
	@ResponseBody	
	public Integer getCount(@PathVariable Integer id) {
		return lessonRepository.countByCategory(id);		
	}

}
