package mn.educenter.app.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import mn.educenter.app.repository.CategoryRepository;
import mn.educenter.app.repository.LessonRepository;
import mn.educenter.app.repository.StudentRepository;
import mn.educenter.app.repository.TeacherRepository;

@Controller
@RequestMapping(value = "/admin")
public class Dashboard {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private LessonRepository lessonRepository;
	
	@Autowired
	private TeacherRepository teacherRepository;
	
	@Autowired
	private StudentRepository studentRepository;
	
	@GetMapping
	public String main(Model model) {
		 model.addAttribute("countCat", categoryRepository.count());
		 model.addAttribute("countTeacher", teacherRepository.count());
		 model.addAttribute("countStudent", studentRepository.count());
		 model.addAttribute("countLesson", lessonRepository.count());
        return "admin";
	}

	

}
