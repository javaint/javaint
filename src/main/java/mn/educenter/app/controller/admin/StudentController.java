package mn.educenter.app.controller.admin;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.educenter.app.domain.Student;
import mn.educenter.app.modal.StudentForm;
import mn.educenter.app.repository.StudentRepository;

@Controller
@RequestMapping(value = "/admin/student")

public class StudentController {
	@Autowired
	private StudentRepository studentRepository;

	@GetMapping
	public String main() {
		return "admin/student";
	}
	
	@GetMapping("list")
	public String list(Model model, @RequestParam(required = false) String fname, Pageable pageable) {
		model.addAttribute("student", studentRepository.findByName(fname == null ? "" : fname, pageable));
		return "admin/student/list";
	}

	@GetMapping("list2selection")
	public String list2selection (Model model, @RequestParam(required = false) String name, Pageable pageable) {		
		model.addAttribute("student", studentRepository.findAll());
		return "admin/student/list2selection";		
	}
	
	@GetMapping("new")
	public String newForm(Map<String, Object> model) {
		model.put("jspform", new StudentForm());
		return "admin/student/new";
	}

	@PostMapping("saveNew")
	public String saveNew(StudentForm form) {
		Student student;
		student = new Student();
		student.setFname(form.getFname());
		student.setLname(form.getLname());
		student.setPhone(form.getPhone());
		student.setRegister(form.getRegister());
		student.setAddress(form.getAddress());
		student.setEmail(form.getEmail());
		student.setProfession(form.getProfession());
		studentRepository.save(student);
		
		return "redirect:/admin/student";
	}
	
	@PostMapping("saveEdit")
	public String save(StudentForm form) {
		Student student;
		student = studentRepository.findOne(form.getId());
		student.setFname(form.getFname());
		student.setLname(form.getLname());
		student.setPhone(form.getPhone());
		student.setRegister(form.getRegister());
		student.setAddress(form.getAddress());
		student.setEmail(form.getEmail());
		student.setProfession(form.getProfession());
		studentRepository.save(student);
		
		return "redirect:/admin/student";
	}

	@GetMapping("{id}/edit")
	public String edit(@PathVariable Integer id, Map<String, Object> model) {
		Student student = studentRepository.findOne(id);
		StudentForm studentForm = new StudentForm();
		studentForm.setId(student.getId());
		studentForm.setFname(student.getFname());
		studentForm.setLname(student.getLname());
		studentForm.setPhone(student.getPhone());
		studentForm.setRegister(student.getRegister());
		studentForm.setEmail(student.getEmail());
		studentForm.setProfession(student.getProfession());
		model.put("jspform", studentForm);
		return "admin/student/edit";
	}

	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		Student student = studentRepository.findOne(id);
		studentRepository.delete(student);

	}

}
