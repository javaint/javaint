package mn.educenter.app.controller.admin;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.educenter.app.domain.Category;
import mn.educenter.app.domain.Lesson;
import mn.educenter.app.domain.Lessonselect;
import mn.educenter.app.domain.LessonselectItem;
import mn.educenter.app.domain.NewClass;
import mn.educenter.app.domain.Selection;
import mn.educenter.app.domain.Student;
import mn.educenter.app.modal.LessonselectForm;
import mn.educenter.app.modal.NewClassForm;
import mn.educenter.app.repository.LessonRepository;
import mn.educenter.app.repository.LessonselectItemRepository;
import mn.educenter.app.repository.LessonselectRepository;
import mn.educenter.app.repository.SelectionRepository;
import mn.educenter.app.repository.StudentRepository;

@Controller
@RequestMapping(value = "/admin/lessonselect")

public class LessonselectController {
	
	@Autowired
	private LessonselectRepository lselectRepository;
	
	@Autowired
	private LessonRepository lessonRepository;
	
	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private SelectionRepository selectionRepo;
	
	@Autowired
	private LessonselectItemRepository lselectItemRepository;
	

	@GetMapping
	public String main() {
		return "admin/lessonselect";
	}

	
	@GetMapping("list")
	public String list(Model model, @RequestParam(required = false) String name, Pageable pageable) {
		model.addAttribute("lselect", lselectRepository.findByLessonName(name == null ? "" : name, pageable));
		return "admin/lessonselect/list";
	}

	@PostMapping("save")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void save(@RequestParam Integer lselect_id, Principal principal) {

//	Lessonselect lselect = new Lessonselect();
//		
//		lselect.setLesson(lesson_id == null ? null : lessonRepository.findOne(lesson_id));
////		lesson.setWeekday(form.getWeekday());
////		lesson.setStart_date(new SimpleDateFormat("yyyy-MM-dd").parse(form.getStart_date()));
////		lesson.setEnd_date(new SimpleDateFormat("yyyy-MM-dd").parse(form.getEnd_date()));
////		lesson.setStart_hour(form.getStart_hour());
////		lesson.setEnd_hour(form.getEnd_hour());
//        lselectRepository.save(lselect);
        
		Lessonselect lselect ;
		lselect = lselectRepository.findOne(lselect_id);
		List<Selection> selections = selectionRepo.findByLselectId(lselect_id);
		for (Selection item : selections) {
			LessonselectItem lselectItem = new LessonselectItem();
			lselectItem.setLessonselect(lselectRepository.findOne(lselect.getId()));
			lselectItem.setStudent(item.getStudent());
					
			lselectItemRepository.save(lselectItem);
			
			
			selectionRepo.delete(item.getId());
		}
	}
	
	@GetMapping("listselection")
	public String listBasket (Model model, Pageable pageable) {
		model.addAttribute("selection", selectionRepo.findAll(pageable));
		return "admin/selection/listselection";		
	}
	
 	@GetMapping("itemlist")
	public String list (Model model, @RequestParam(required = false) Integer id, Pageable pageable) {
		model.addAttribute("lselectItem", lselectItemRepository.findByLessonselect(id, pageable)); 
		return "admin/lessonselect/itemlist";		
	}
	    
	    
    @GetMapping("newClass")
	public String newForm (@RequestParam(required = false) Integer lesson_id, Map<String, Object> model) {
    	model.put("jspform", new NewClassForm());
		/*model.put("lesson", lessonRepository.findOne(id));*/
		return "admin/lessonselect/newClass";	
	}
    
    @PostMapping("saveClass")
	public String saveClass(@Valid @ModelAttribute("jspform")NewClassForm form, @RequestParam(value = "lesson_id", required =false) Lesson lesson_id) throws ParseException {
		
		Lessonselect newclass ;
		
		newclass= new Lessonselect();
		
		newclass.setLesson(lesson_id);
		newclass.setWeekday(form.getWeekday());
		newclass.setStart_date(new SimpleDateFormat("yyyy-MM-dd").parse(form.getStart_date()));
		newclass.setEnd_date(new SimpleDateFormat("yyyy-MM-dd").parse(form.getEnd_date()));
		newclass.setStart_hour(form.getStart_hour());
		newclass.setEnd_hour(form.getEnd_hour());
		
		lselectRepository.save(newclass);
		
		return "admin/lessonselect";	
	}
    
    @GetMapping("classlist")
	public String classlist (Model model, @RequestParam(required = false) Integer id, Pageable pageable) {
		model.addAttribute("lselect", lselectRepository.findByLessonselect(id, pageable)); 
		return "admin/lessonselect/classlist";		
	}

	/*@GetMapping("{id}/edit")
	public String edit(@PathVariable Integer id, Map<String, Object> model) {
		Lessonselect lselect = lselectRepository.findOne(id);
		LessonselectForm lselectForm = new LessonselectForm();
		lselectForm.setId(lselect.getId());
		lselectForm.setLesson_id(lselect.getLesson() == null ? null : lselect.getLesson().getId());
		lselectForm.setStudent_id(lselect.getStudent() == null ? null : lselect.getStudent().getId());
		model.put("lesson", lessonRepository.findAll());
		model.put("student", studentRepository.findAll());
		model.put("jspform", lselectForm);
		return "admin/lessonselect/edit";
	}
*/
	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		Lessonselect lselect = lselectRepository.findOne(id);
		lselectRepository.delete(lselect);

	}
	

}
