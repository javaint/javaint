package mn.educenter.app.controller.admin;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.educenter.app.domain.Category;
import mn.educenter.app.domain.Lessonselect;
import mn.educenter.app.domain.LessonselectItem;
import mn.educenter.app.domain.Selection;
import mn.educenter.app.domain.Student;
import mn.educenter.app.modal.LessonselectForm;
import mn.educenter.app.repository.LessonRepository;
import mn.educenter.app.repository.LessonselectItemRepository;
import mn.educenter.app.repository.LessonselectRepository;
import mn.educenter.app.repository.SelectionRepository;
import mn.educenter.app.repository.StudentRepository;

@Controller
@RequestMapping(value = "/admin/lessonselectItem")

public class LessonselectItemController {
	
	@Autowired
	private LessonselectRepository lselectRepository;
	
	@Autowired
	private LessonRepository lessonRepository;
	
	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private SelectionRepository selectionRepo;
	
	@Autowired
	private LessonselectItemRepository lselectItemRepository;
	

	@GetMapping
	public String main() {
		return "admin/lessonselectItem";
	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		LessonselectItem lselectItem = lselectItemRepository.findOne(id);
		lselectItemRepository.delete(lselectItem);

	}

	

	

}
