package mn.educenter.app.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class AdminController {
	
	@GetMapping
	@RequestMapping (value ="/admin")
	public String main () {		
		return "admin";		
	}
	
	
	
	
}
