package mn.educenter.app.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "lessonselect_item")
public class LessonselectItem {
	@Id
	@GeneratedValue
	private Integer id;
	@ManyToOne
	private Lessonselect lessonselect;
	@ManyToOne
	private Student student;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Lessonselect getLessonselect() {
		return lessonselect;
	}

	public void setLessonselect(Lessonselect lessonselect) {
		this.lessonselect = lessonselect;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	

}
