package mn.educenter.app.business;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Calendar;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import mn.educenter.app.domain.Image;
import mn.educenter.app.domain.RawFile;

@Service
public class FileService {

	@Value("${file.root-path}")
	private String rootPath;

	public RawFile uploadFile(MultipartFile file) throws Exception {
		String relativeDir = "files" + File.separator + RandomStringUtils.randomAlphabetic(10);

		String fullDir = rootPath + File.separator + relativeDir;
		File dir = new File(fullDir);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		String fileDir = fullDir + File.separator + file.getOriginalFilename();
		File target = new File(fileDir);
		file.transferTo(target);

		RawFile rawFile = new RawFile();
		rawFile.setPath(relativeDir + File.separator + file.getOriginalFilename());
		rawFile.setSize(file.getSize());
		return rawFile;
	}
	
	public Boolean isImageFile(MultipartFile file) {
		String type = file.getContentType();
		String imageFormats = "image/jpeg;image/gif;image/png";
		if (imageFormats.contains(type)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public Image uploadImage(MultipartFile file) throws Exception {
		Calendar now = Calendar.getInstance();

		String relativeDir = "images" 
				+ File.separator + now.get(Calendar.YEAR) 
				+ File.separator + (now.get(Calendar.MONTH) + 1) 
				+ File.separator + RandomStringUtils.randomAlphabetic(10);

		String fullDir = rootPath + File.separator + relativeDir;
		File dir = new File(fullDir);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		String fileDir = fullDir + File.separator + file.getOriginalFilename();
		File target = new File(fileDir);	
		file.transferTo(target);
		
		BufferedImage bimg = ImageIO.read(target);
		
		Image image = new Image();
		image.setPath(relativeDir + File.separator + file.getOriginalFilename());
		image.setDescription(file.getOriginalFilename());
		image.setSize(file.getSize());			
		image.setWidth(bimg.getWidth());
		image.setHeight(bimg.getHeight());
		return image;
	}

}
