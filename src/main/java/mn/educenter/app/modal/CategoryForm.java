package mn.educenter.app.modal;

import org.hibernate.validator.constraints.NotBlank;

public class CategoryForm {

	private Integer id;
	@NotBlank(message = "Хоосон байж болохгүй")
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
