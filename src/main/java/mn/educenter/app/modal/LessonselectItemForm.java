package mn.educenter.app.modal;

import java.util.Date;

public class LessonselectItemForm {

	private Integer id;
	private Integer lesson_id;
	private Integer lessonselect_id;
	private String start_date;
	private String end_date;
	private String start_hour;
	private String end_hour;
	private String weekday;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLesson_id() {
		return lesson_id;
	}

	public void setLesson_id(Integer lesson_id) {
		this.lesson_id = lesson_id;
	}

	public Integer getLessonselect_id() {
		return lessonselect_id;
	}

	public void setLessonselect_id(Integer lessonselect_id) {
		this.lessonselect_id = lessonselect_id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getStart_hour() {
		return start_hour;
	}

	public void setStart_hour(String start_hour) {
		this.start_hour = start_hour;
	}

	public String getEnd_hour() {
		return end_hour;
	}

	public void setEnd_hour(String end_hour) {
		this.end_hour = end_hour;
	}

	public String getWeekday() {
		return weekday;
	}

	public void setWeekday(String weekday) {
		this.weekday = weekday;
	}
	

}
