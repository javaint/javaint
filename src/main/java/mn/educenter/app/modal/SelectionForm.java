package mn.educenter.app.modal;

public class SelectionForm {

	private Integer id;
	private Integer lselect_id;
	private Integer student_id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLselect_id() {
		return lselect_id;
	}

	public void setLselect_id(Integer lselect_id) {
		this.lselect_id = lselect_id;
	}

	public Integer getStudent_id() {
		return student_id;
	}

	public void setStudent_id(Integer student_id) {
		this.student_id = student_id;
	}

}
