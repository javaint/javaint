package mn.educenter.app.preparer;

import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.PreparerException;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import mn.educenter.app.domain.Lesson;
import mn.educenter.app.repository.LessonRepository;

@Component
public class LessonPreparer implements ViewPreparer {
	
	@Autowired
	private LessonRepository lessonRepository;

	public void execute(Request tilesRequest, AttributeContext attributeContext) throws PreparerException {
		Integer size = Integer.parseInt(attributeContext.getLocalAttribute("size").toString()); 
		PageRequest pageRequest = new PageRequest(0, size, Sort.Direction.DESC, "id");
		Page<Lesson> lessons = lessonRepository.findAll(pageRequest);
		attributeContext.putAttribute("lessons", new Attribute(lessons.getContent()));
	}
}
