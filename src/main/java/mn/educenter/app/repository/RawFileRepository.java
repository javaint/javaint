package mn.educenter.app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.educenter.app.domain.RawFile;



public interface RawFileRepository extends PagingAndSortingRepository<RawFile, Integer> {
	
}
