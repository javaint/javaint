package mn.educenter.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import mn.educenter.app.domain.Teacher;

public interface TeacherRepository extends CrudRepository <Teacher, Integer> {
	
	@Query("select t from Teacher t where t.fname like %?1%")	
	Page<Teacher> findByName(String fname, Pageable pageable);

}
