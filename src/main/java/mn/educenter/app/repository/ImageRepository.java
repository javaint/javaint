package mn.educenter.app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import mn.educenter.app.domain.Image;

public interface ImageRepository extends PagingAndSortingRepository<Image, Integer> {
	
}