package mn.educenter.app.repository;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.educenter.app.domain.LessonselectItem;
import mn.educenter.app.domain.NewClass;

public interface NewClassRepository extends PagingAndSortingRepository <NewClass, Integer> {

	@Query("select n from NewClass n where n.start_date like %?1%")
	Page<NewClass> findByStartDate(Date start_date, Pageable pageable);
	
	@Query("select n from NewClass n where n.lesson_id=?1")
	Page<NewClass> findByClass(Integer lesson_id, Pageable pageable);
	/*
	@Query("select s from Selection s where s.lesson.weekday=?1 and s.lesson.start_hour=?2")	
	List<Selection> findByCond(String weekday , String start_hour);
	
	@Query("select l from Lesson l where l.start_hour=?1")	
	List<Lesson> findByStarthour(String start_hour);
	
	@Query("select l from Lesson l where l.end_hour=?1")	
	List<Lesson> findByEndhour(String end_hour);*/
	
}
