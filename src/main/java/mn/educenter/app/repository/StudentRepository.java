package mn.educenter.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.educenter.app.domain.Student;

public interface StudentRepository extends PagingAndSortingRepository <Student, Integer> {

	@Query("select s from Student s where s.fname like %?1%")	
	Page<Student> findByName(String fname, Pageable pageable);
	
}
