package mn.educenter.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.educenter.app.domain.Lesson;
import mn.educenter.app.domain.Selection;

public interface SelectionRepository extends PagingAndSortingRepository <Selection, Integer> {

	@Query("select s from Selection s where s.lselect_id=?1")
	List<Selection> findByLselectId(Integer lselect_id);
	
	@Query("select s from Selection s where s.student.id=?1 and s.lselect_id=?2")
	List<Selection> find(Integer student_id, Integer lselect_id );
	/*
	@Query("select s from Selection s where s.lesson.weekday=?1 and s.lesson.start_hour=?2")	
	List<Selection> findByCond(String weekday , String start_hour);
	
	@Query("select l from Lesson l where l.start_hour=?1")	
	List<Lesson> findByStarthour(String start_hour);
	
	@Query("select l from Lesson l where l.end_hour=?1")	
	List<Lesson> findByEndhour(String end_hour);*/
	
}
