package mn.educenter.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import mn.educenter.app.domain.Authority;



public interface AuthorityRepository extends CrudRepository<Authority, Integer> {
	
	@Query("select a from Authority a where a.username=?1")	
	List<Authority> findByUsername(String username);
	
	@Query("select a from Authority a where a.authority=?1")	
	List<Authority> findByAuth(String authority);
	
}
