package mn.educenter.app.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.educenter.app.domain.Category;
import mn.educenter.app.domain.Lesson;

public interface CategoryRepository extends PagingAndSortingRepository <Category, Integer> {

	@Query("select c from Category c where c.name like %?1%")	
	Page<Category> findByName(String name, Pageable pageable);
	
	@Query("select c from Category c where c.name=?1")	
	List<Category> findByCatName(String name);
}
