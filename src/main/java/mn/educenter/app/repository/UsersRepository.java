package mn.educenter.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.educenter.app.domain.Users;

public interface UsersRepository extends PagingAndSortingRepository <Users, String> {

	@Query("select u from Users u where u.username like %?1%")	
	Page<Users> findByName(String username, Pageable pageable);
	
}
