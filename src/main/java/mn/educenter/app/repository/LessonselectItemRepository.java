package mn.educenter.app.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.educenter.app.domain.LessonselectItem;

public interface LessonselectItemRepository extends PagingAndSortingRepository <LessonselectItem, Integer> {
	
	@Query("select l from LessonselectItem l where l.lessonselect.id=?1")
	Page<LessonselectItem> findByLessonselect(Integer lessonselect_id, Pageable pageable);
	
	
}
