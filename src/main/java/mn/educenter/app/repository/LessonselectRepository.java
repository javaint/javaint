package mn.educenter.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.educenter.app.domain.Lessonselect;
import mn.educenter.app.domain.LessonselectItem;

public interface LessonselectRepository extends PagingAndSortingRepository <Lessonselect, Integer> {
	
	@Query("select l from Lessonselect l where l.lesson.name like %?1%")	
	Page<Lessonselect> findByLessonName(String name, Pageable pageable);
	
	@Query("select l from Lessonselect l where l.lesson.id=?1")
	Page<Lessonselect> findByLessonselect(Integer lesson_id, Pageable pageable);
	
	

}
