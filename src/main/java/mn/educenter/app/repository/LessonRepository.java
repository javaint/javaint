package mn.educenter.app.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.educenter.app.domain.Lesson;

public interface LessonRepository extends PagingAndSortingRepository <Lesson, Integer> {


	@Query("select l from Lesson l where l.name like %?1%")	
	Page<Lesson> findByName(String name, Pageable pageable);
	
	@Query("select l from Lesson l where l.category.id=?1")
	Page<Lesson> findByCategory(Integer category_id, Pageable pageable);
	
	@Query("select l from Lesson l where l.category.id=?1")
	List<Lesson> findByCategoryId(Integer category_id);
	
	@Query("select l from Lesson l where l.category.id = ?1 and l.name like %?2% ")	
	List<Lesson> findByCategory(Integer category_id, String name);
	
	@Query("select l from Lesson l where l.weekday=?1")	
	List<Lesson> findByWeekday(String weekday);
	
	@Query("select l from Lesson l where l.start_hour=?1")	
	List<Lesson> findByStarthour(String start_hour);
	
	@Query("select l from Lesson l where l.end_hour=?1")	
	List<Lesson> findByEndhour(String end_hour);

	@Query("select count(l) from Lesson l where l.category.id=?1")
	Integer countByCategory(Integer category_id);	
	
	@Query("select count(l) from Lesson l where l.teacher.id=?1")
	Integer countByTeacher(Integer teacher_id);
	
	
	
}
