
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>


	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th style="text-align:center">Сонгогдсон сурагчид</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${selections}" var="selection">
					<tr>
						<td><i class="fas fa-minus-circle mr-4" style="color:red" onclick="deleteThis(${selection.id})">
						</i>${selection.student.fname}</td>				
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

<script>

	var paginate = function (page) {
		loadList(page, $('#filterName').val());
	}  
	
	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/selection/"+id,			  			 
				  type: 'DELETE',
				  success: function() {
					  alert("Амжиллтай устлаа");
					  loadList();			  	   
				  }
			});	
		}				
	}
</script>

