<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="d-flex justify-content-between align-items-center">
	<h3><i class="fas fa-clipboard-check"></i> Хичээл сонголт</h3>
	<button class="button add-btn" onclick="submit();"><i class="far fa-calendar-plus"></i></button>
</div>

<form class="form-inline">
	<label class="sr-only" for="inlineFormInputName2">Нэр</label> <input
		type="text" class="form-control mb-2 mr-sm-2" id="filterName"
		placeholder="Нэр">
	<button type="button" class="btn btn-dark mb-2" onclick="searchList();">Хайх</button>
</form>

<div class="row">
	<div class="col-md-6">
		<div id="listStudent"></div>
	</div>
	<div class="col-md-6">
		<div id="chooselist"></div>
	</div>
</div>

<script>
var searchList = function() {
	
	listBooksOfAllCategory($('#filterName').val());
}

var loadStudent= function() {
	$.get("/admin/student/list2selection", function(data) {
		$("#listStudent").html(data);
	});
}
loadStudent();

var addThis = function(id) {
	$.get("/admin/selection/save?lselect_id=${param.lselect_id}&student_id="+ id, function(data) {
		if (data==='duplicate'){
			alert('Энэ сурагч аль хэдийн сонгогдсон');	
		}
		else if (data==='success'){
			loadList();	
		}			 
	})
}

var loadList = function() {
	$.get("/admin/selection/list?lselect_id=${param.lselect_id}", function(data) {
		$("#chooselist").html(data);
	});
}
loadList();


var submit = function() {
	$.post("/admin/lessonselect/save?lselect_id=${param.lselect_id}&${_csrf.parameterName}=${_csrf.token}",
		function(data) {
			window.location = "/admin/lessonselect"
		});
}

	
</script>