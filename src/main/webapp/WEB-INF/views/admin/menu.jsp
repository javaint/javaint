<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="menu">
	<div class="container">
		 <div class="menu_list">
		 	<ul class="nav">
		 		<li><a href="/admin" ><i class="fas fa-cog"></i>Нүүр</a></li>
		 		<li><a href="/admin/users"><i class="fas fa-user"></i>Хэрэглэгч</a></li>
		 		<li><a href="/admin/category"><i class="fas fa-sitemap"></i>Ангилал</a></li>
		 		<li><a href="/admin/lesson"><i class="fas fa-book-reader"></i>Хичээл</a></li>
		 		<li><a href="/admin/teacher"><i class="fas fa-chalkboard-teacher"></i>Багш</a></li>
		 		<li> <a href="/admin/student"><i class="fas fa-user-graduate"></i>Сурагч</a></li>
		 		<li><a href="/admin/lessonselect"><i class="fas fa-clipboard-check"></i>Хичээл сонголт</a></li>
		 		<li><a href="/admin/image"><i class="fas fa-images"></i>Зураг оруулах</a></li>
		 	</ul>
         </div>
	</div>
</div>
<script>
console.log(window.location.pathname);
jQuery(document).ready(function($){
	var path = window.location.pathname;
	
	if (path == '/admin'){
		path = '/admin';
	} else if (path == '/admin/users'){
		path = '/admin/users';
	} else if (path == '/admin/category'){
		path = '/admin/category';
	} else if (path == '/admin/lesson'){
		path = '/admin/lesson';
	} else if (path == '/admin/teacher'){
		path = '/admin/teacher';
	} else if (path == '/admin/student'){
		path = '/admin/student';
	} else if (path == '/admin/lessonselect'){
		path = '/admin/lessonselect';
	} else if (path == '/admin/image'){
		path = '/admin/image';
	}
	
	var target = $('.menu_list a[href="'+path+'"]');
	target.addClass('active');
})
</script>