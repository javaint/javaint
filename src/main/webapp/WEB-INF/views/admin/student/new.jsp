<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="d-flex justify-content-between align-items-center index_title">
	<h2>Сурагчийн мэдээлэл засах</h2>
	<div>
		<button type="button" data-toggle="tooltip" title="Хадгалах" class="btn btn-outline-success" onclick="$('#editForm').submit();"><i class="fas fa-save"></i></button>
		<a href="/admin/student" class="btn btn-outline-dark" data-toggle="tooltip" title="Буцах"><i class="fas fa-undo-alt"></i></a>
	</div>
</div>
<form:form modelAttribute="jspform" action="/admin/student/saveNew" id="editForm">
	<form:hidden path="id" />
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<form:label path="fname">Сурагчын нэр</form:label>
				<form:input path="fname" class="form-control" />
			</div>
			<div class="col-md-6">
				<form:label path="lname">Сурагчын овог</form:label>
				<form:input path="lname" class="form-control" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<form:label path="register">Регистерийн дугаар</form:label>
				<form:input path="register" class="form-control" />
			</div>
			<div class="col-md-6">
				<form:label path="phone">Утасны дугаар</form:label>
				<form:input path="phone" class="form-control" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-4">
				<form:label path="address">Гэрийн хаяг</form:label>
				<form:input path="address" class="form-control" />
			</div>
			<div class="col-md-4">
				<form:label path="email">Имэйл</form:label>
				<form:input path="email" class="form-control" />
			</div>
			<div class="col-md-4">
				<form:label path="profession">Мэргэжил</form:label>
		<form:input path="profession" class="form-control" />
			</div>
		</div>
	</div>
</form:form>