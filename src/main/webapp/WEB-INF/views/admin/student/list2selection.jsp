<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="table-responsive">
	<table class="table table-bordered">
		<tbody>
			<c:forEach items="${student}" var="student">
				<tr> 
					<td onclick="addThis(${student.id})" style="cursor: pointer; user-select: none;">${student.fname}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
