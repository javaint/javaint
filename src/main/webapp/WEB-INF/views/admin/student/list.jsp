<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${student.numberOfElements eq 0}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table">
			<thead>
				<tr>
					<th style="width: 1px;">№</th>
					<th>Сурагчын нэр</th>	
					<th>Сурагчын овог</th>	
					<th>Регистрийн дугаар</th>	
					<th>Утасны дугаар</th>	
					<th>Гэрийн хаяг</th>	
					<th>И-мэйл хаяг</th>	
					<th>Үндсэн мэргэжил</th>						
					<th style="width: 1px;"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${student.content}" var="student" varStatus="status">
					<tr>
						<th><c:out value="${param.page*param.size+status.count}"/></th>
						<td>${student.fname}</td>
						<td>${student.lname}</td>
						<td>${student.register}</td>
						<td>${student.phone}</td>
						<td>${student.address}</td>
						<td>${student.email}</td>
						<td>${student.profession}</td>					
						<td style="white-space: nowrap;">
							<c:if test="${param.select eq 1}">
						       <a class="button	chose-btn btn btn-outline-warning" style="width:40px;" href="/admin/selection?lesson_id=${lesson.id}"></a>
						   	</c:if> 
							<a href="/admin/student/${student.id}/edit" data-toggle="tooltip" title="Дэлгэрэнгүй харах" class="btn btn-outline-warning"><i class="fas fa-share-square"></i></a>
							<button class="btn btn-outline-danger" type="button" data-toggle="tooltip" title="Устгах"
								onclick="deleteThis(${student.id})"><i class="fas fa-trash-alt"></i></button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:if test="${not (student.first && student.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not student.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${student.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${student.totalPages}">
						<li
							class="page-item <c:if test="${student.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not student.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${student.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if>
	</c:otherwise>
</c:choose>

<script>


var deleteThis = function (id) {
	if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
		$.ajax({
			  url: "/admin/student/"+id,			  			 
			  type: 'DELETE',
			  success: function() {
				  alert("Амжиллтай устлаа");
				  loadList(0, "");			  	   
			  }
		});	
	}				
}
</script>
