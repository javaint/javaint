<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>


<div class="d-flex justify-content-between align-items-center index_title">
	<h2>Хичээл нэмэх/засах</h2>
	<div>
		<button type="button" data-toggle="tooltip" title="Хадгалах" class="btn btn-outline-success saveBtn" onclick="$('#editForm').submit();"><i class="fas fa-save"></i></button>
		<button type="button" data-toggle="tooltip" title="Засварлах" class="btn btn-outline-warning" id="editThis"><i class="fas fa-edit"></i></button>
		<a href="/admin/lesson" class="btn btn-outline-dark" data-toggle="tooltip" title="Буцах"><i class="fas fa-undo-alt"></i></a>
	</div>
</div>
<form:form modelAttribute="jspform" action="/admin/lesson/saveEdit" id="editForm">
	<form:hidden path="id" />
	<div class="form-group">
		<div id="image"></div>
		<button type="button" id="loadBtn" class="btn btn-light" onclick="loadImages(0);">Зураг сонгох</button>
		<form:hidden path="imageId" />
		<div id="images"></div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<form:label path="category_id">Хичээлийн ангилал</form:label>
				<form:select path="category_id" cssClass="form-control" cssErrorClass="form-control is-invalid">
					<form:option value="0" label="..." />
					<form:options items="${categories}" itemValue="id" itemLabel="name" />
				</form:select>
				<form:errors path="category_id" cssClass="invalid-feedback" element="div" />
			</div>
			<div class="col-md-6">
				<form:label path="teacher_id">Багш</form:label>
				<form:select path="teacher_id" class="form-control">
					<form:option value="0" label="..." />
					<form:options items="${teacher}" itemValue="id" itemLabel="fname" />
				</form:select>
			</div>
		</div>	
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<form:label path="name">Хичээлийн нэр</form:label>
				<form:input type="text" path="name" class="form-control"/>
			</div>
			<div class="col-md-6">
				<form:label path="weekday">Хичээлийн өдрүүд</form:label><br/>
				<c:choose>
					<c:when test="${fn:contains(jspform.weekday, 'Даваа')}">
						<form:checkbox path="weekday" value="Даваа"  checked="checked"/>Даваа
					</c:when>
					<c:otherwise>
						<form:checkbox path="weekday" value="Даваа" />Даваа
					</c:otherwise>
				</c:choose>		
				<c:choose>
					<c:when test="${fn:contains(jspform.weekday, 'Мягмар')}">
						<form:checkbox path="weekday" value="Мягмар"  checked="checked"/>Мягмар
					</c:when>
					<c:otherwise>
						<form:checkbox path="weekday" value="Мягмар" />Мягмар
					</c:otherwise>
				</c:choose>	
				<c:choose>
					<c:when test="${fn:contains(jspform.weekday, 'Лхагва')}">
						<form:checkbox path="weekday" value="Лхагва"  checked="checked"/>Лхагва	
					</c:when>
					<c:otherwise>
						<form:checkbox path="weekday" value="Лхагва" />Лхагва
					</c:otherwise>
				</c:choose>	
				<c:choose>
					<c:when test="${fn:contains(jspform.weekday, 'Пүрэв')}">
						<form:checkbox path="weekday" value="Пүрэв"  checked="checked"/>Пүрэв
					</c:when>
					<c:otherwise>
						<form:checkbox path="weekday" value="Пүрэв" />Пүрэв
					</c:otherwise>
				</c:choose>	
				<c:choose>
					<c:when test="${fn:contains(jspform.weekday, 'Баасан')}">
						<form:checkbox path="weekday" value="Баасан"  checked="checked"/>Баасан<br/>	
					</c:when>
					<c:otherwise>
						<form:checkbox path="weekday" value="Баасан" />Баасан<br/>
					</c:otherwise>
				</c:choose>			
				<form:errors path="weekday" cssClass="invalid-feedback" element="div" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<form:label path="start_date">Эхлэх өдөр</form:label>
				<form:input type="date" path="start_date" class="form-control"/>
			</div>
			<div class="col-md-6">
				<form:label path="end_date">Дуусах өдөр</form:label>
				<form:input type="date" path="end_date" class="form-control" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<form:label path="start_hour">Эхлэх цаг</form:label>
				<form:input id="time" type="text" path="start_hour" cssClass="form-control" cssErrorClass="form-control is-invalid"/>
				<form:errors path="start_hour" cssClass="invalid-feedback" element="div" />
			</div>
			<div class="col-md-6">
				<form:label path="end_hour">Дуусах цаг</form:label>
				<form:input id="time1" type="text" path="end_hour" class="form-control"/>
			</div>
		</div>
	</div>
	<div class="form-group">
		<form:label path="description">Товч тайлбар</form:label>
		<form:input path="description" class="form-control"/>
	</div>
	<div class="form-group">
		<form:label path="text">Дэлгэрэнгүй мэдээлэл</form:label>
		<form:textarea path="text" class="form-control"/>
	</div>
	
</form:form>
<div class="d-flex justify-content-end align-items-right">
	<div>
		<button type="button" data-toggle="tooltip" title="Хадгалах" class="btn btn-outline-success saveBtn" onclick="$('#editForm').submit();"><i class="fas fa-save"></i></button>
		<button type="button" data-toggle="tooltip" title="Засварлах" class="btn btn-outline-warning" id="editThis"><i class="fas fa-edit"></i></button>
		<a href="/admin/lesson" class="btn btn-outline-dark" data-toggle="tooltip" title="Буцах"><i class="fas fa-undo-alt"></i></a>
	</div>
</div>
<script>

$(".saveBtn").hide();

$("#editThis").click(function(){ 

  $(".saveBtn").show();

});

var editor = CKEDITOR.replace('text');

$('input, textarea, checkbox, select, option, #loadBtn').attr('disabled', true);

$(document).ready(function()
	{
	 $('#editThis').click(function()
	 {
	   $("input, textarea, checkbox, select, option, #loadBtn").removeAttr("disabled");  
	 });

});

$(document).ready(function(){
	 $('#time').timepicker({
		 timeFormat: 'h:mm p',
        interval: 30,
        minTime: '10',
        maxTime: '9:00pm',
        startTime: '10:00'
    });
});
$(document).ready(function(){
	 $('#time1').timepicker({
		 timeFormat: 'h:mm p',
       interval: 30,
       minTime: '10',
       maxTime: '9:00pm',
       startTime: '10:00'
   });
});

var loadImages = function(page) {
	$.get("/admin/image/select?sort=created,desc&size=10&page="+page, function(data) {
		$("#images").html(data);
	});
}
var selectImage = function(id) {
	$("#imageId").val(id);
	$.get("/admin/image/select/"+id, function(data) {
		$("#image").html(data);
	});		
	$("#images").html("");
}

<c:if test="${!empty jspform.imageId}">
	selectImage(${jspform.imageId});
</c:if>	
</script> 