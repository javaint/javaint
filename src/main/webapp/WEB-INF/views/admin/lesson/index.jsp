
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="d-flex justify-content-between align-items-center index_title">
	<h3> <i class="fas fa-book-reader mr-3 fa-les"></i>Хичээл</h3>
	<a href="/admin/lesson/new" class="btn btn-outline-light" data-toggle="tooltip" title="Шинээр нэмэх"><i class="far fa-plus-square"></i></a>
</div>

<form class="search" >
  <input type="search" placeholder="Хичээлийн нэрээр хайх..." required id="filterName">
  <button type="button" id="myBtn" onclick="searchList();">Хайх</button>
</form> 
<div id="list"></div>
<div id="more"></div>





<script>

	var searchList = function() {
		loadList(0, $('#filterName').val());
	}
	
	var loadList = function(page, name) {
		$.get("/admin/lesson/list?page=" + page
				+ "&category_id=${param.category_id}&size=8&sort=id,desc&name=" + name, function(data) {
			$("#list").html(data);
		});
	}
	loadList(0, "");
	
	var paginate = function (page) {
		loadList(page, $('#filterName').val());
	} 
	
	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/lesson/" + id,			  			 
				  type: 'DELETE',
				  success: function() {
					  alert("Амжиллтай устлаа");
					  loadList(0, "");			  	   
				  }
			});	
		}				
	}

	var selectLesson = function(id) {
		$("#lesson_id").val(id);
		$.get("/admin/lesson/"+id, function(data) {
			$("#lessons").html(data);
		});		
		$("#lessons").html("");
	}
	
	var more = function(page, id) {
   		$.get("/admin/lessonselect/classlist?page=" + page
				+ "&size=10&id=" + id , function(data) {
   			$("#more").html(data);
   		});
   	 }
</script>