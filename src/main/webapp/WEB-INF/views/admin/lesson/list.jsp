<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:choose>
	<c:when test="${lesson.numberOfElements eq 0}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table">
			<thead>
				<tr>
					<th style="width: 1px;">№</th>
					<th style="width: 1px;">Зураг</th>
					<th>Хичээлийн нэр</th>	
					<th>Хичээлийн ангилал</th>
					<th>Багш</th> 	
					<!-- <th>Хичээлийн цагийн хуваарь</th> 	
					<th>Хичээллэх өдрүүд</th>	 -->		
					
					<th style="width: 1px;"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lesson.content}" var="lesson" varStatus="status">
					<tr>
						<th><c:out value="${param.page*param.size+status.count}"/></th>
						<td>
							<c:if test="${!empty lesson.image}">
								<img src="/${lesson.image.path}" class="img-thumbnail rounded" style="max-width: 100px ; "/>
							</c:if>												
						</td>
						<td>${lesson.name}</td>
						<td>${lesson.category.name}</td>
						<td>${lesson.teacher.fname}</td>
						<%-- <td>${lesson.start_hour}-${lesson.end_hour}</td>
						<td>						
							<c:forEach items="${lesson.weekday}" var="current">
								<c:out value="${current}" />
							</c:forEach> 
						</td> --%>
												
						<td style="white-space: nowrap;">
							<%-- <c:if test="${param.select ne 0}">
 						       <a class="button	chose-btn btn btn-outline-warning"  href="/admin/selection?lesson_id=${lesson.id}"><i class="far fa-plus-square"></i></a>
 						   	</c:if> --%>
							<button class="button edit-btn" type="button" data-toggle="modal" title="Дэлгэрэнгүй харах"data-target=".bd-example-modal-xl"
								onclick="more(0,${lesson.id})"><i class="fas fa-edit"></i></button>
							<a class="button	chose-btn btn btn-outline-warning"  href="/admin/lessonselect/newClass?lesson_id=${lesson.id}"><i class="far fa-plus-square"></i></a>
							<%-- <button class="btn btn-outline-warning" type="button" data-toggle="tooltip" title="Анги нэмэх"
									onclick="createClass(${lesson.id});"><i class="far fa-plus-square"></i></button>  --%>
							<a href="/admin/lesson/${lesson.id}/edit" data-toggle="tooltip" title="Дэлгэрэнгүй харах" class="btn btn-outline-warning"><i class="fas fa-share-square"></i></a>
							<button class="btn btn-outline-danger" type="button" data-toggle="tooltip" title="Устгах"
								onclick="deleteThis(${lesson.id})"><i class="fas fa-trash-alt"></i></button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:if test="${not (lesson.first && lesson.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not lesson.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${lesson.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${lesson.totalPages}">
						<li
							class="page-item <c:if test="${lesson.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not lesson.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${lesson.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if>
	</c:otherwise>
</c:choose>

<script>
</script>