<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${not (images.first && images.last)}">
	<nav class="d-flex flex-row-reverse">
		<ul class="pagination">
			<c:if test="${not images.first}">
				<li class="page-item"><a class="page-link" href="#"
					onclick="loadList(${images.number-1});">Өмнөх</a></li>
			</c:if>
			<c:if test="${not images.last}">
				<li class="page-item"><a class="page-link" href="#"
					onclick="loadList(${images.number+1});">Дараах</a></li>
			</c:if>
		</ul>
	</nav>
</c:if>

<c:choose>
	<c:when test="${images.numberOfElements eq 0}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<div class="image__list zoom-gallery">
		<c:forEach items="${images.content}" var="image">
			<div class="feature__image">
				<button class="button image-del-btn" type="button" onclick="deleteThis(${image.id})"><i class="fas fa-times "></i></button> 
				<a href="/${image.path}" class="list_image image-source-link" download>
				
					<div class="feature__image-item">
						
						<img src="/${image.path}" class="image__thumb"/>
					</div>
				</a> 
				
					
			</div>
		</c:forEach>
		</div>
	</c:otherwise>
</c:choose>


<script>
	$(document).ready(function() {
		$('.zoom-gallery').magnificPopup({
			delegate: 'a',
			type: 'image',
			closeOnContentClick: false,
			closeBtnInside: false,
			mainClass: 'mfp-with-zoom mfp-img-mobile',
			image: {
				verticalFit: true,
				titleSrc: function(item) {
					return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
				}
			},
			gallery: {
				enabled: true
			},
			zoom: {
				enabled: true,
				duration: 300, // don't foget to change the duration also in CSS
				opener: function(element) {
					return element.find('img');
				}
			}
			
		});
	});
	
	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/image/" + id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
				  type: 'DELETE',
				  success: function() {
					  alert("Амжиллтай устлаа");
					  loadList(0,""
							  );			  	   
				  }
			});	
		}				
	}
</script>