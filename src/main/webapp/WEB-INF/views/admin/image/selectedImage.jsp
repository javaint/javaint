<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<img src="/${image.path}" class="img-thumbnail rounded d-block mr-2 mb-2" style="max-width: auto; height: 100px;"/>