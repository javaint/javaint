<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${teacher.numberOfElements eq 0}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table">
			<thead>
				<tr>
					<th style="width: 1px;">№</th>
					<th style="width: 1px;">Зураг</th>
					<th>Багш дээр бүртгэлтэй хичээл</th>	
					<th>Багшийн Овог</th>		
					<th>Багшийн нэр</th>	
					<th>Мэргэжил</th>	
					<th>Регистрийн дугаар</th>	
					<th>Утасны дугаар</th>	
					<th>И-мэйл</th>				
					<th style="width: 1px;"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${teacher.content}" var="teacher" varStatus="status">
					<tr>
						<th><c:out value="${param.page*param.size+status.count}"/></th>
						<td>
							<c:if test="${!empty teacher.image}">
								<img src="/${teacher.image.path}" class="img-thumbnail rounded" style="max-width: 100px ; "/>
							</c:if>												
						</td>
						<td>${teacher.counter}</td>
						<td>${teacher.fname}</td>
						<td>${teacher.lname}</td>
						<td>${teacher.profession}</td>
						<td>${teacher.phone}</td>
						<td>${teacher.register}</td>
						<td>${teacher.email}</td>					
						<td style="white-space: nowrap;">

							<a href="/admin/teacher/${teacher.id}/edit" data-toggle="tooltip" title="Дэлгэрэнгүй харах" class="btn btn-outline-warning"><i class="fas fa-share-square"></i></a>
							<button class="btn btn-outline-danger" type="button" data-toggle="tooltip" title="Устгах"
								onclick="deleteThis(${teacher.id})"><i class="fas fa-trash-alt"></i></button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:if test="${not (teacher.first && teacher.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not teacher.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${teacher.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${teacher.totalPages}">
						<li
							class="page-item <c:if test="${teacher.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not teacher.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${teacher.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if>
	</c:otherwise>
</c:choose>

