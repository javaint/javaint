<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="d-flex justify-content-between align-items-center index_title">
	<h2>Багшийн мэдээлэл засах</h2>
	<div>
		<button type="button" data-toggle="tooltip" title="Хадгалах" class="btn btn-outline-success saveBtn" onclick="$('#editForm').submit();"><i class="fas fa-save"></i></button>
		<button type="button" data-toggle="tooltip" title="Засварлах" class="btn btn-outline-warning" id="editThis"><i class="fas fa-edit"></i></button>
		<a href="/admin/teacher" class="btn btn-outline-dark" data-toggle="tooltip" title="Буцах"><i class="fas fa-undo-alt"></i></a>
	</div>
</div>
<form:form modelAttribute="jspform" action="/admin/teacher/saveEdit" id="editForm">
	<form:hidden path="id" />
	<div class="form-group">
		<div id="image"></div>
		<button type="button" id="loadBtn" class="btn btn-light" onclick="loadImages(0);">Зураг
			сонгох</button>
		<form:hidden path="imageId" />
		<div id="images"></div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<form:label path="fname">Багшийн нэр</form:label>
				<form:input path="fname" class="form-control" />
			</div>
			<div class="col-md-6">
				<form:label path="lname">Багшийн овог</form:label>
				<form:input path="lname" class="form-control" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<form:label path="profession">Мэргэжил</form:label>
				<form:input path="profession" class="form-control" />
			</div>
			<div class="col-md-6">
				<form:label path="register">Регистрийн дугаар</form:label>
				<form:input path="register" class="form-control" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<form:label path="phone">Утасны дугаар</form:label>
				<form:input path="phone" class="form-control" />
			</div>
			<div class="col-md-6">
				<form:label path="email">И-мэйл</form:label>
				<form:input path="email" class="form-control" />
			</div>
		</div>
	</div>
</form:form>
<div class="d-flex justify-content-end align-items-right">
	<div>
		<button type="button" data-toggle="tooltip" title="Хадгалах" class="btn btn-outline-success saveBtn" onclick="$('#editForm').submit();"><i class="fas fa-save"></i></button>
		<button type="button" data-toggle="tooltip" title="Засварлах" class="btn btn-outline-warning" id="editThis"><i class="fas fa-edit"></i></button>
		<a href="/admin/teacher" class="btn btn-outline-dark" data-toggle="tooltip" title="Буцах"><i class="fas fa-undo-alt"></i></a>
	</div>
</div>
<script>
$(".saveBtn").hide();

$("#editThis").click(function(){ 

  $(".saveBtn").show();

});


	$('input, textarea, checkbox, select, option, #loadBtn').attr('disabled', true);
	
	$(document).ready(function()
		{
		 $('#editThis').click(function()
		 {
		   $("input, textarea, checkbox, select, option, #loadBtn").removeAttr("disabled");  
		 });
	
	});

	var loadImages = function(page) {
		$.get("/admin/image/select?sort=created,desc&size=10&page="+page, function(data) {
			$("#images").html(data);
		});
	}
	var selectImage = function(id) {
		$("#imageId").val(id);
		$.get("/admin/image/select/"+id, function(data) {
			$("#image").html(data);
		});		
		$("#images").html("");
	}
	
	<c:if test="${!empty jspform.imageId}">
		selectImage(${jspform.imageId});
	</c:if>	
</script>