<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="d-flex justify-content-between align-items-center index_title">
	<h3> <i class="fas fa-book-reader mr-3 fa-les"></i>Багш нар</h3>
	<a href="/admin/teacher/new" class="btn btn-outline-light" data-toggle="tooltip" title="Шинээр нэмэх"><i class="far fa-plus-square"></i></a>
</div>

<form class="search" >
  <input type="search" placeholder="Багшийн нэрээр хайх..." required id="filterName">
  <button type="button" onclick="searchList();">Хайх</button>
</form> 

<div id="list"></div>

<div id="editModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" id="edit"></div>
</div>

<script>
	var searchList = function() {
		loadList(0, $('#filterName').val());
	}
	var loadList = function(page, fname) {
		$.get("/admin/teacher/list?page=" + page
				+ "&size=8&sort=id,desc&fname=" + fname, function(data) {
			$("#list").html(data);
		});
	}
	loadList(0, "");
	
	var paginate = function (page) {
		loadList(page, $('#filterName').val());
	} 
	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/teacher/"+id,			  			 
				  type: 'DELETE',
				  success: function() {
					  alert("Амжиллтай устлаа");
					  loadList(0, "");			  	   
				  }
			});	
		}				
	}

	
</script>