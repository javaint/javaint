<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title">Шинээр анги нэмэх</h5>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
		<form:form modelAttribute="jspform" id="editForm">
			<form:hidden path="id" />
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<form:label path="lesson_id">Хичээлийн ангилал</form:label>
						<form:select path="lesson_id" cssClass="form-control" cssErrorClass="form-control is-invalid">
							<form:option value="0" label="..." />
							<form:options items="${lesson}" itemValue="id" itemLabel="name" />
						</form:select>
						<form:errors path="category_id" cssClass="invalid-feedback" element="div" />
					</div>
					<div class="col-md-6">
						<form:label path="weekday">Хичээлийн өдрүүд</form:label><br/>
						<c:choose>
							<c:when test="${fn:contains(jspform.weekday, 'Даваа')}">
								<form:checkbox path="weekday" value="Даваа"  checked="checked"/>Даваа
							</c:when>
							<c:otherwise>
								<form:checkbox path="weekday" value="Даваа" />Даваа
							</c:otherwise>
						</c:choose>		
						<c:choose>
							<c:when test="${fn:contains(jspform.weekday, 'Мягмар')}">
								<form:checkbox path="weekday" value="Мягмар"  checked="checked"/>Мягмар
							</c:when>
							<c:otherwise>
								<form:checkbox path="weekday" value="Мягмар" />Мягмар
							</c:otherwise>
						</c:choose>	
						<c:choose>
							<c:when test="${fn:contains(jspform.weekday, 'Лхагва')}">
								<form:checkbox path="weekday" value="Лхагва"  checked="checked"/>Лхагва	
							</c:when>
							<c:otherwise>
								<form:checkbox path="weekday" value="Лхагва" />Лхагва
							</c:otherwise>
						</c:choose>	
						<c:choose>
							<c:when test="${fn:contains(jspform.weekday, 'Пүрэв')}">
								<form:checkbox path="weekday" value="Пүрэв"  checked="checked"/>Пүрэв
							</c:when>
							<c:otherwise>
								<form:checkbox path="weekday" value="Пүрэв" />Пүрэв
							</c:otherwise>
						</c:choose>	
						<c:choose>
							<c:when test="${fn:contains(jspform.weekday, 'Баасан')}">
								<form:checkbox path="weekday" value="Баасан"  checked="checked"/>Баасан<br/>	
							</c:when>
							<c:otherwise>
								<form:checkbox path="weekday" value="Баасан" />Баасан<br/>
							</c:otherwise>
						</c:choose>			
						<form:errors path="weekday" cssClass="invalid-feedback" element="div" />
				
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<form:label path="start_date">Эхлэх өдөр</form:label>
						<form:input type="date" path="start_date" class="form-control"/>
					</div>
					<div class="col-md-6">
						<form:label path="end_date">Дуусах өдөр</form:label>
						<form:input type="date" path="end_date" class="form-control" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<form:label path="start_hour">Эхлэх цаг</form:label>
						<form:input id="time" type="text" path="start_hour" cssClass="form-control" cssErrorClass="form-control is-invalid"/>
						<form:errors path="start_hour" cssClass="invalid-feedback" element="div" />
					</div>
					<div class="col-md-6">
						<form:label path="end_hour">Дуусах цаг</form:label>
						<form:input id="time1" type="text" path="end_hour" class="form-control"/>
					</div>
				</div>
			</div>
		</form:form>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Болих</button>
		<button type="button" class="btn btn-primary" onclick="submitForm();">Хадгалах</button>
	</div>
</div>