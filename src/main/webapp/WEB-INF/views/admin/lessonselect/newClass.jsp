<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>


<div class="d-flex justify-content-between align-items-center index_title">
	<h2>Ангийн тухай мэдээлэл оруулах</h2>
	<div>
		<button type="button" data-toggle="tooltip" title="Хадгалах" class="btn btn-outline-success saveBtn" onclick="$('#editForm').submit();"><i class="fas fa-save"></i></button>
		<a href="/admin/newClass" class="btn btn-outline-dark" data-toggle="tooltip" title="Буцах"><i class="fas fa-undo-alt"></i></a>
	</div>
</div>
<form:form modelAttribute="jspform" action="/admin/lessonselect/saveClass?lesson_id=${param.lesson_id }" id="editForm">
	<form:hidden path="id" />
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				
			</div>
			<div class="col-md-6">
				<form:label path="weekday">Хичээлийн өдрүүд</form:label><br/>
				<c:choose>
					<c:when test="${fn:contains(jspform.weekday, 'Даваа')}">
						<form:checkbox path="weekday" value="Даваа"  checked="checked"/>Даваа
					</c:when>
					<c:otherwise>
						<form:checkbox path="weekday" value="Даваа" />Даваа
					</c:otherwise>
				</c:choose>		
				<c:choose>
					<c:when test="${fn:contains(jspform.weekday, 'Мягмар')}">
						<form:checkbox path="weekday" value="Мягмар"  checked="checked"/>Мягмар
					</c:when>
					<c:otherwise>
						<form:checkbox path="weekday" value="Мягмар" />Мягмар
					</c:otherwise>
				</c:choose>	
				<c:choose>
					<c:when test="${fn:contains(jspform.weekday, 'Лхагва')}">
						<form:checkbox path="weekday" value="Лхагва"  checked="checked"/>Лхагва	
					</c:when>
					<c:otherwise>
						<form:checkbox path="weekday" value="Лхагва" />Лхагва
					</c:otherwise>
				</c:choose>	
				<c:choose>
					<c:when test="${fn:contains(jspform.weekday, 'Пүрэв')}">
						<form:checkbox path="weekday" value="Пүрэв"  checked="checked"/>Пүрэв
					</c:when>
					<c:otherwise>
						<form:checkbox path="weekday" value="Пүрэв" />Пүрэв
					</c:otherwise>
				</c:choose>	
				<c:choose>
					<c:when test="${fn:contains(jspform.weekday, 'Баасан')}">
						<form:checkbox path="weekday" value="Баасан"  checked="checked"/>Баасан<br/>	
					</c:when>
					<c:otherwise>
						<form:checkbox path="weekday" value="Баасан" />Баасан<br/>
					</c:otherwise>
				</c:choose>			
				<form:errors path="weekday" cssClass="invalid-feedback" element="div" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<form:label path="start_date">Эхлэх өдөр</form:label>
				<form:input type="date" path="start_date" class="form-control"/>
			</div>
			<div class="col-md-6">
				<form:label path="end_date">Дуусах өдөр</form:label>
				<form:input type="date" path="end_date" class="form-control" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<form:label path="start_hour">Эхлэх цаг</form:label>
				<form:input id="time" type="text" path="start_hour" cssClass="form-control" cssErrorClass="form-control is-invalid"/>
				<form:errors path="start_hour" cssClass="invalid-feedback" element="div" />
			</div>
			<div class="col-md-6">
				<form:label path="end_hour">Дуусах цаг</form:label>
				<form:input id="time1" type="text" path="end_hour" class="form-control"/>
			</div>
		</div>
	</div>
	
</form:form>
<script>


$(document).ready(function(){
	 $('#time').timepicker({
		 timeFormat: 'h:mm p',
        interval: 30,
        minTime: '10',
        maxTime: '9:00pm',
        startTime: '10:00'
    });
});
$(document).ready(function(){
	 $('#time1').timepicker({
		 timeFormat: 'h:mm p',
       interval: 30,
       minTime: '10',
       maxTime: '9:00pm',
       startTime: '10:00'
   });
});


</script> 