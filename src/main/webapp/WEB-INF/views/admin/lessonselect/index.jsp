<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="d-flex justify-content-between align-items-center">
	<h3><i class="fas fa-clipboard-check"></i>Хичээлийн хуваарь</h3>
	<a href="/admin/lesson?select=1" class="btn btn-dark"><i class="far fa-plus-square"></i></a>
</div>

<form class="form-inline">
	<label class="sr-only" for="inlineFormInputName2">Нэр</label> <input
		type="text" class="form-control mb-2 mr-sm-2" id="filterName"
		placeholder="Нэр">
	<button type="button" class="btn btn-dark mb-2" onclick="searchList();">Хайх</button>
</form>


<div class="row">
	<div class="col-md-6">
		<div id="list"></div>
	</div>
	<div class="col-md-6">
		<div id="more"></div>
	</div>
</div>

<div id="editModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" id="edit"></div>
</div>

<script>
	var searchList = function() {
		loadList(0, $('#filterName').val());
	}
	
	var loadList = function() {
		$.get("/admin/lessonselect/list", function(data) {
			$("#list").html(data);
		});
	}
	loadList();
	

	var more = function(page, id) {
   		$.get("/admin/lessonselect/itemlist?page=" + page
				+ "&size=10&id=" + id , function(data) {
   			$("#more").html(data);
   		});
   	 }
	
</script>