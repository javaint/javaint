
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<div class="modal content ">
	<div class="modal-header">
		<h5 class="modal-title">Ангилал нэмэх/засах</h5>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
		
	</div>
</div>
<%-- <c:choose>
	<c:when test="${lselectItem.numberOfElements eq 0}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise> --%>
		<table class="table">
			<thead>
				<tr>
					<th>№</th>	
					<th>Хичээлийн нэр</th>		
					<th>Хичээллэх өдрүүд</th>	
					<th>Хичээлийн цаг</th>	
					<th>Хичээл эхлэх өдөр</th>	
					<th>Хичээл дуусах өдөр</th>	
					<th style="width: 1px;"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lselect.content}" var="lselect" varStatus="status">
					<tr>
						<th>
							<c:out value="${param.page*param.size+status.count}"/>
						</th> 
						<td>${lselect.lesson.name}</td>	
						<td>${lselect.lesson.weekday}</td>	
						<td>${lselect.lesson.start_hour} - ${lselect.lesson.end_hour}</td>	
						<td>${lselect.lesson.start_date}</td>	
						<td>${lselect.lesson.end_date}</td>		
						<td style="white-space: nowrap;">
						<button class="button del-btn" type="button"
								onclick="deleteThis(${lselectItem.id})"><i class="fas fa-trash-alt"></i></button>
						<a class="button	chose-btn btn btn-outline-warning"  href="/admin/selection?lselect_id=${lselect.id}"><i class="far fa-plus-square"></i></a>
<%-- 
							<button class="button edit-btn" type="button" data-toggle="modal" data-target=".bd-example-modal-sm"
								onclick="more(${lselect.id})"><i class="fas fa-edit"></i></button>
							<button class="button del-btn" type="button"
								onclick="deleteThis(${lselect.id})"><i class="fas fa-trash-alt"></i></button> --%>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	<%-- <c:if test="${not (lselectItem.first && lselectItem.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not lselectItem.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${lselectItem.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${lselectItem.totalPages}">
						<li
							class="page-item <c:if test="${lselectItem.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not lselectItem.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${lselectItem.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if>
	</c:otherwise>
</c:choose> --%>

<script>

	var paginate = function (page) {
		loadList(page, $('#filterName').val());
	}  
	
	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/lessonselectItem/"+id,			  			 
				  type: 'DELETE',
				  success: function() {
					  alert("Амжиллтай устлаа");
					  more(0, "");			  	   
				  }
			});	
		}				
	}
	
</script>

