<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="dashboard">

	<ul>
		<li>
			<div class="dash__item">
				<div class="dash__main">
					<div class="dash__left">
						<i class="fas fa-sitemap"></i>
					</div>
					<div class="dash__right">
						<span class="title">Нийт ангилал</span><br>
						<span class="count">${countCat}</span>
					</div>
				</div>
				<div class="dash__more">
					<a href="/admin/category">Дэлгэрэнгүй</a>
				</div>
			</div>
		</li>
		<li>
			<div class="dash__item">
				<div class="dash__main">
					<div class="dash__left">
						<i class="fas fa-book-reader"></i>
					</div>
					<div class="dash__right">
						<span class="title">Нийт хичээл</span><br>
						<span class="count">${countLesson}</span>
					</div>
				</div>
				<div class="dash__more">
					<a href="/admin/lesson">Дэлгэрэнгүй</a>
				</div>
			</div>
		</li>
		<li>
			<div class="dash__item">
				<div class="dash__main">
					<div class="dash__left">
						<i class="fas fa-chalkboard-teacher"></i>
					</div>
					<div class="dash__right">
						<span class="title">Нийт багш нар</span><br>
						<span class="count">${countTeacher}</span>
					</div>
				</div>
				<div class="dash__more">
					<a href="/admin/teacher">Дэлгэрэнгүй</a>
				</div>
			</div>
		</li>
		<li>
			<div class="dash__item">
				<div class="dash__main">
					<div class="dash__left">
						<i class="fas fa-user-graduate"></i>
					</div>
					<div class="dash__right">
						<span class="title">Нийт сурагчид</span><br>
						<span class="count">${countStudent}</span>
					</div>
				</div>
				<div class="dash__more">
					<a href="/admin/student">Дэлгэрэнгүй</a>
				</div>
			</div>
		</li>
	</ul>
</div>

<script>
	
</script>