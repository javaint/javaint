<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="d-flex justify-content-between align-items-center index_title">
	<h2>Ангилал нэмэх/засах</h2>
	<div>
		<button type="button" data-toggle="tooltip" title="Хадгалах" class="btn btn-outline-success" onclick="$('#newForm').submit();"><i class="fas fa-save"></i></button>
		<a href="/admin/category" class="btn btn-outline-dark" data-toggle="tooltip" title="Буцах"><i class="fas fa-undo-alt"></i></a>
	</div>
</div>
<form:form modelAttribute="jspform" action="/admin/category/saveNew" id="newForm">
	<form:hidden path="id" />
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="form-group">
				<form:label path="name">Ангилалын нэр</form:label>
				<form:input path="name" cssClass="form-control" cssErrorClass="form-control is-invalid"/>
				<form:errors path="name" cssClass="invalid-feedback" element="div" />
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
</form:form>
