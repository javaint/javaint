<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${categories.numberOfElements eq 0}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th style="width: 1px;">№</th>
					<th>Ангилалын нэр</th>
					<th>Ангилалд хамааралтай хичээлийн тоо</th>					
					<th style="width: 1px;"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${categories.content}" var="category" varStatus="status">
					<tr>
						<th><c:out value="${param.page*param.size+status.count}"/></th>
						<td>${category.name}</td>	
						<c:choose>
							<c:when test="${category.counter ne 0}">
								<td>Яг одоо <span style="font-weight: bold; font-size: 20px;">${category.counter}</span> хичээл энэ ангилалд бүртгэлтэй байна</td>
							</c:when>
							<c:otherwise>
								<td>Энэ ангилалд бүртгэлтэй хичээл байхгүй</td>
							</c:otherwise>
						</c:choose>			
						<td style="white-space: nowrap;">

							<a href="/admin/category/${category.id}/edit" data-toggle="tooltip" title="Дэлгэрэнгүй харах" class="btn btn-outline-warning"><i class="fas fa-share-square"></i></a>
							<button class="btn btn-outline-danger" type="button" data-toggle="tooltip" title="Устгах"
								onclick="deleteThis(${category.id})"><i class="fas fa-trash-alt"></i></button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
		<c:if test="${not (categories.first && category.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not categories.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${categories.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${categories.totalPages}">
						<li
							class="page-item <c:if test="${categories.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not categories.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${categories.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if>
	</c:otherwise>
</c:choose>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body">
				<p>
					Энэ ангилал дээр <span id="deleteCount">0</span> хичээл бүртгэлтэй
					байна. 
				</p>
			</div>
			<div class="modal-footer">
				<a href="" class="btn btn-primary" id="deleteLink">Дэлгэрэнгүй</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>
<script>
var deleteThis = function (id) {
	if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
		$.ajax({
			  url: "/admin/category/"+id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
			  type: 'DELETE',
			  success: function(message) {
				  if (message=="Unpossible") {
					  $.get('/admin/category/'+id+'/count', function(count){
						//  alert(count + "ном ашиглагдсан байна");
						$('#deleteCount').text(count);
						$('#deleteLink').attr('href', '/admin/lesson?category_id=' + id);
						  $("#myModal").modal();
					  });
					  
				  }
				  else {
					  alert("Амжиллтай устлаа");
					  loadList(0, "");
				  }			  
				  			  	   
			  }
		});	
	}				
}
</script>