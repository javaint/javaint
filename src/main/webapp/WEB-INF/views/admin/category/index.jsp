<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="d-flex justify-content-between align-items-center index_title">
	<h3> <i class="fas fa-book-reader mr-3 fa-les"></i>Ангилал</h3>
	<a href="/admin/category/new" class="btn btn-outline-light" data-toggle="tooltip" title="Шинээр нэмэх"><i class="far fa-plus-square"></i></a>
</div>


<form class="search" >
  <input type="search" placeholder="Ангилалын нэрээр хайх..." required id="filterName">
  <button type="button" onclick="searchList();">Хайх</button>
</form>  
<div id="list"></div>

<div id="editModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" id="edit"></div>
</div>

<script>
	var searchList = function() {
		loadList(0, $('#filterName').val());
	}

	var loadList = function(page, name) {
		$.get("/admin/category/list?page=" + page 
				+ "&size=10&sort=id,desc&name=" + name, function(data) {
			$("#list").html(data);
		});
	}
	loadList(0, "");
	
	var paginate = function (page) {
		loadList(page, $('#filterName').val());
	} 
	
</script>