<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<c:forEach items="${categories}" var="category">
	<div class="accordion">
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" type="button"
						style="color: #144271;" data-toggle="collapse"
						data-target="#collapseOne${category.id}">${category.name}</button>
				</h5>
			</div>
			<div id="collapseOne${category.id}" class="collapse">
				<div class="card-body" id="lesson${category.id}"></div>
			</div>
		</div>
	</div>
	
</c:forEach>

<script>
	var listBooksOfAllCategory = function(name) {
		
		<c:forEach items="${categories}" var="category">
		
		$.get("/admin/lesson/list2selection?category_id=${category.id}&name=" + name, function(data) {
			$("#lesson${category.id}").html(data);
		});
		
		</c:forEach>
		
	}
	
	listBooksOfAllCategory("");
</script>
<%-- <c:choose>
	<c:when test="${categories.numberOfElements eq 0}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table">
			<thead>
				<tr>
					<th style="width: 1px;">№</th>
					<th>Ангилалын нэр</th>
					<th>Ангилалд хамааралтай хичээлийн тоо</th>	
					<th>Статус</th>						
					<th style="width: 1px;"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${categories.content}" var="category" varStatus="status">
					<tr>
						<th><c:out value="${param.page*param.size+status.count}"/></th>
						<td>${category.name}</td>	
						<c:choose>
							<c:when test="${category.counter ne 0}">
								<td>Яг одоо <span style="font-weight: bold; font-size: 20px;">${category.counter}</span> хичээл энэ ангилалд бүртгэлтэй байна</td>
							</c:when>
							<c:otherwise>
								<td>Энэ ангилалд бүртгэлтэй хичээл байхгүй</td>
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${category.status eq 'ENABLED'}">
								<td><label class="container"><input
										type="checkbox" path="status" checked="checked" onclick="disableThis(${category.id})"
										value="${category.status}"><span class="checkmark"></span></label></td>
							</c:when>
							<c:otherwise>
								<td><label class="container"><input
										type="checkbox" path="status" value="${category.status}"  onclick="enableThis(${category.id})"><span
										class="checkmark"></span></label></td>
							</c:otherwise>
						</c:choose>				
						<td style="white-space: nowrap;">

							<a href="/admin/category/${category.id}/edit" data-toggle="tooltip" title="Дэлгэрэнгүй харах" class="btn btn-outline-warning"><i class="fas fa-share-square"></i></a>
							<button class="btn btn-outline-danger" type="button" data-toggle="tooltip" title="Устгах"
								onclick="deleteThis(${category.id})"><i class="fas fa-trash-alt"></i></button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:if test="${not (categories.first && category.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not categories.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${categories.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${categories.totalPages}">
						<li
							class="page-item <c:if test="${categories.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not categories.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${categories.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if>
		


	</c:otherwise>
</c:choose> --%>

