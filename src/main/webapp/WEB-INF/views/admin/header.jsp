<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<header>
	<div class="header">
      <div class="container">
      	<div class="row">
      		<div class="col-md-2 header_logo">
      			<a class="navbar-brand" href="/admin">EduCenter</a>
      		</div>
      		<div class="col-md-6"></div>
      		
      			<sec:authentication var="users" property="principal"/>
      		<div class="col-md-4 header_login">
      			<ul >
      			
		            <li style="margin-bottom: 12px;"><div style="float: left; margin-bottom: 12px;"><a href="/admin">Welcome, ${users.username}</a></div></li>
		            <li>
			            <form action="/logout" method="post">
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
							<div class="navigation">
								<a class="logoutBtn" href="/logout">
							  	<i class="fas fa-power-off"></i>
							 		 <div class="logout">ГАРАХ</div>
								</a>
							  
							</div>
						</form>
					</li>
		          </ul>
      		</div>
      	</div>
      </div>
	</div>
</header>

