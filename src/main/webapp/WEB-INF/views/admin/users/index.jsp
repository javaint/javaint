<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="d-flex justify-content-between align-items-center index_title">
	<h3> <i class="fas fa-user mr-3 fa-les"></i>Хэрэглэгч</h3>
	<a href="/admin/users/new" class="btn btn-outline-light" data-toggle="tooltip" title="Шинээр нэмэх"><i class="far fa-plus-square"></i></a>
</div>
<form class="search" >
  <input type="search" placeholder="Хэрэглэгчийн нэрээр хайх..." required id="filterName">
  <button type="button" onclick="searchList();">Хайх</button>
</form> 
<div id="list"></div>

<div id="editModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" id="edit"></div>
</div>

<script>

	var searchList = function() {
		loadList(0, $('#filterName').val());
	}

	var loadList = function(page, username) {
		$.get("/admin/users/list?page=" + page 
				+ "&size=10&sort=id,desc&username=" + username, function(data) {
			$("#list").html(data);
		});
	}
	loadList(0, "");

	
	var deleteThis = function(id) {
		if (confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				url : "/admin/users/" + id
						+ "?${_csrf.parameterName}=${_csrf.token}",
				type : 'DELETE',
				success : function() {
					alert("Амжиллтай устлаа");
					loadList();
				}
			});
		}
	}

	var showRole = function(username) {
		$('#editModal').modal('show');
		$.get("/admin/authority/list?username=" + username, function(data) {
			$("#edit").html(data);
		})
	}

</script>