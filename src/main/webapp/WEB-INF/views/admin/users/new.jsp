<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="d-flex justify-content-between align-items-center index_title">
	<h2>Хэрэглэгч нэмэх/засах</h2>
	<div>
		<button type="button" data-toggle="tooltip" title="Хадгалах" class="btn btn-outline-success" onclick="$('#editForm').submit();"><i class="fas fa-save"></i></button>
		<a href="/admin/users" class="btn btn-outline-dark" data-toggle="tooltip" title="Буцах"><i class="fas fa-undo-alt"></i></a>
	</div>
</div>
<form:form modelAttribute="jspform" action="/admin/users/saveEdit" id="editForm">			
	<div class="row">
		<div class="col-md-5">
			<div class="form-group">
				<form:label path="username">Нэр</form:label>
				<form:input path="username" class="form-control" />
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<form:label path="password">Нууц үг</form:label>
				<form:input type="password" path="password" class="form-control" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">				
				<form:checkbox path="enabled" label="Идэвхтэй" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<form:label path="fname">Хэрэглэгчийн нэр</form:label>
				<form:input type="text" path="fname" class="form-control" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<form:label path="lname">Хэрэглэгчийн овог</form:label>
				<form:input type="text" path="lname" class="form-control" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<form:label path="email">И-мэйл</form:label>
				<form:input type="text" path="email" class="form-control" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<form:label path="phone">Утасны дугаар</form:label>
				<form:input type="text" path="phone" class="form-control" />
			</div>
		</div>
	</div>
</form:form>