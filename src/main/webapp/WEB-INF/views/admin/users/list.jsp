<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${users.numberOfElements eq 0}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table">
			<thead>
				<tr>
					<th>№</th>
					<th>Нэвтрэх нэр</th>
					<th>Хэрэглэгчийн нэр</th>
					<th>Хэрэглэгчийн овог</th>
					<th>И-Мэйл</th>
					<th>Утасны дугаар</th>
					<th style="width: 1px;"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${users.content}" var="user" varStatus="status">
					<tr>
						<th><c:out value="${param.page*param.size+status.count}"/></th>
						<td>${user.username}</td>
						<td>${user.fname}</td>
						<td>${user.lname}</td>
						<td>${user.email}</td>
						<td>${user.phone}</td>
						<td style="white-space: nowrap;">
							<button class="btn btn-light" type="button"
								onclick="showRole('${user.username}')">Эрх нэмэх</button>
							<a href="/admin/users/${user.username}/edit" data-toggle="tooltip" title="Дэлгэрэнгүй харах" class="btn btn-outline-warning"><i class="fas fa-share-square"></i></a>
							<a href="/admin/users/${user.username}/editPass" data-toggle="tooltip" title="Нууц үг солих" class="btn btn-outline-info"><i class="fas fa-edit"></i></a>
							<button class="btn btn-outline-danger" type="button" data-toggle="tooltip" title="Устгах"
								onclick="deleteThis(${user.username})"><i class="fas fa-trash-alt"></i></button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:if test="${not (users.first && users.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not users.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${users.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${users.totalPages}">
						<li
							class="page-item <c:if test="${users.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not users.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${users.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if>
	</c:otherwise>
</c:choose>


