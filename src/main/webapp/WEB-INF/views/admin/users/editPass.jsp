<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="d-flex justify-content-between align-items-center index_title">
	<h2>Нууц үг солих</h2>
	<div>
		<button type="button" data-toggle="tooltip" title="Хадгалах" class="btn btn-outline-success" onclick="$('#editForm').submit();"><i class="fas fa-save"></i></button>
		<a href="/admin/users" class="btn btn-outline-dark" data-toggle="tooltip" title="Буцах"><i class="fas fa-undo-alt"></i></a>
	</div>
</div>
<form:form modelAttribute="jspform" action="/admin/users/savePass" id="editForm">			
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-4">
			<div class="form-group">
				<form:label path="username">Нэр</form:label>
				<form:input type="text" id="username" path="username" class="form-control" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<form:label path="password">Нууц үг</form:label>
				<form:input type="password" path="password" class="form-control" />
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
</form:form>