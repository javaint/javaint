<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<div class="d-flex justify-content-between align-items-center">
	<h3><i class="fas fa-clipboard-check"></i> Шинэ анги </h3>
	<button class="button add-btn" onclick="submit();"><i class="far fa-calendar-plus"></i></button>
</div>

<form class="form-inline">
	<label class="sr-only" for="inlineFormInputName2">Нэр</label> <input
		type="text" class="form-control mb-2 mr-sm-2" id="filterName"
		placeholder="Нэр">
	<button type="button" class="btn btn-dark mb-2" onclick="searchList();">Хайх</button>
</form>
<a href="/admin/newClass/new?lesson_id=${param.lesson_id }" class="btn btn-dark"><i class="far fa-plus-square"></i></a>
<div class="row">
	<div class="col-md-6">
		 Хичээлийн нэр: ${newClass.name}
	</div>
	<div class="col-md-6">
		
	</div>
</div>

<script>
var searchList = function() {
	
	listBooksOfAllCategory($('#filterName').val());
}

var newForm= function() {
	$.get("/admin/newClass/new", function(data) {
		$("#newform").html(data);
	});
}
newForm();

var addThis = function(id) {
	$.get("/admin/selection/save?lesson_id=${param.lesson_id}&student_id="+ id, function(data) {
		if (data==='duplicate'){
			alert('Энэ сурагч аль хэдийн сонгогдсон');	
		}
		else if (data==='success'){
			loadList();	
		}			 
	})
}

var loadList = function() {
	$.get("/admin/selection/list?lesson_id=${param.lesson_id}", function(data) {
		$("#chooselist").html(data);
	});
}
loadList();


var submit = function() {
	$.post("/admin/lessonselect/save?lesson_id=${param.lesson_id}&${_csrf.parameterName}=${_csrf.token}",
		function(data) {
			window.location = "/admin/lessonselect"
		});
}

	
</script>