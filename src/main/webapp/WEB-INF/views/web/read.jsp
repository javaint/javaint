<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<section class="container">
	<div class="topmain">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<div>
					<c:if test="${!empty lesson.image}">
						<div style="margin-left: 20px;">
							<img class="img-responsive border-grey" style="height: 150px;" src="/${lesson.image.path}" />
						</div>
					</c:if>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<h3 class="text-bold margin-top-0">${lesson.name}</h3>
				<p class="font-bigger text-justify">
					${lesson.description}	
				</p>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<div class="course-due-date text-center margin-bottom-10">
                    <div>
                        <span class="text-center">Бүртгэл дуусах өдөр</span>
                    </div>
                    <div>
                        <span class="text-center">${lesson.start_date}</span>
                    </div>
              		 </div>
              		 <div class="course-start text-center margin-bottom-10">
                    <div>
                        <span class="text-center">Эхлэх өдөр</span>
                    </div>
                    <div>
                        <span class="text-center">${lesson.start_date}</span>
                    </div>
               	</div>
			</div>
		</div>
	</div>
	<div class="midmain">
		<div class="row ">
			<div class="col-md-8 midmain__teacher">
				<div class="teacher ">
					<div class="teacher__title">
						Сургагч багш нар
					</div>
					<div class="teacher__image">
						<c:if test="${!empty lesson.teacher.image}">
							<div style="margin-left: 20px;">
								<img class="img-responsive border-grey" style="height: 150px;" src="/${lesson.teacher.image.path}" />
							</div>
						</c:if>
					</div>
					<div class="teacher__name">
						${lesson.teacher.lname} овогтой ${lesson.teacher.fname} 
					</div>
					<div class="teacher__profession">
						${lesson.teacher.profession}
					</div>
				</div>
				<div class="desc">
					<div class="desc__must">
						${lesson.description}
					</div>
					<div class="desc__text">
						${lesson.text}
					</div>
				</div>
			</div>
			<div class="col-md-4 midmain__detail">
				<div class="detail margin-bottom-20">
					<ul>
						<li><i class="fas fa-calendar-alt"></i>Хичээлийн үргэлжлэх хугацаа: <br/>${lesson.start_date} - ${lesson.end_date} </li>
						<li><i class="fas fa-clock"></i>Цагийн хуваарь: ${lesson.start_hour} - ${lesson.end_hour}</li>
						<li><i class="fas fa-calendar-day"></i>Хичээллэх өдрүүд: <br/>
							<c:choose>
								<c:when test="${lesson.weekday == '1'}">
									Даваа
								</c:when>
								<c:when test="${lesson.weekday == '2'}">
									Мягмар
								</c:when>
								<c:when test="${lesson.weekday == '3'}">
									Лхагва
								</c:when>
								<c:when test="${lesson.weekday == '4'}">
									Пүрэв
								</c:when>
								<c:when test="${lesson.weekday == '5'}">
									Баасан
								</c:when>
								<c:when test="${lesson.weekday == '6'}">
									Даваа, Лхагва, Баасан
								</c:when>
								<c:when test="${lesson.weekday == '7'}">
									Мягмар, Пүрэв
								</c:when>
								<c:when test="${lesson.weekday == '8'}">
									Даваа, Мягмар, Лхагва, Пүрэв, Баасан
								</c:when>	
								<c:otherwise>
									null
								</c:otherwise>
							</c:choose>
						</li>
						<li><i class="fas fa-money-bill-alt"></i>Төлбөр: Үнэ төлбөргүй</li>
						<li><i class="fas fa-tachometer-alt"></i>Төлөв: Бүртгэл явагдаж байна</li>
					</ul>
				</div>
				<div class="reated white-bg margin-bottom-20">
					<h5 class="blue text-bold text-uppercase margin-bottom-20 margin-top-20">Бусад сургалтууд</h5>
					<tiles:insertTemplate template="/WEB-INF/views/web/common/components/lessonsidebar.jsp" preparer="lessonPreparer">
						<tiles:putAttribute name="size" value="5"/>
					</tiles:insertTemplate>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="intro">
	<div class="container">
			<div class="row">
                <div class="col-md-4 col-sm-4 margin-bottom-10">
                    <div>
                        <div class="big-icon margin-bottom-10">
                            <div class="icon-style margin-auto"><i class="fa fa-graduation-cap"></i></div>
                        </div>
                        <div class="meta-text">
                            <h3 class="text-center">Сургалтууд</h3>
                            <p class="text-center">Онолын мэдлэгийг практикт тургуурлан олгох, дадлагажуулах, мэдлэг, чадварыг баталгаажуулан сертификат олгох</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 margin-bottom-10">
                    <div>
                        <div class="big-icon margin-bottom-10">
                            <div class="icon-style margin-auto"><i class="fa fa-desktop"></i></div>
                        </div>
                        <div class="meta-text">
                            <h3 class="text-center">Сургалтын орчин</h3>
                            <p class="text-center">Орчин үеийн компьютер, өндөр хурдны интернет, тав тухтай орчин</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 margin-bottom-10">
                    <div>
                        <div class="big-icon margin-bottom-10">
                            <div class="icon-style margin-auto"><i class="fa fa-users"></i></div>
                        </div>
                        <div class="meta-text">
                            <h3 class="text-center">Сургагч багш нар</h3>
                            <p class="text-center">Чадвартай, заах арга сайтай, албан ёсны сертификаттай, мэргэжлийн багш нар, салбарын шилдэг мэргэжилтэнүүд заана.</p>
                        </div>
                    </div>
                </div>
            </div>
		</div>
</section>
<!-- <div class="container" style="max-width: 500px;">
	<div class="card mb-1">
		<div class="card-body" id="commentNew"></div>
	</div>

	<div id="commentList"></div>
</div>

<script>
	var commentNew = function() {
		$.get("/comments/new/${blog.id}", function(data) {
			$("#commentNew").html(data);
		});
	}
	commentNew();

	var commentList = function() {
		$.get("/comments/list/${blog.id}?sort=created,desc", function(data) {
			$("#commentList").html(data);
		});
	}
	commentList();
</script> -->


					