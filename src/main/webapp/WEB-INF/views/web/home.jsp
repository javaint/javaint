<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<%-- <div class="container mt-5 mb-5" style="max-width: 700px;">

	<tiles:insertTemplate
		template="/WEB-INF/views/web/common/components/lessons.jsp"
		preparer="lessonPreparer">
		<tiles:putAttribute name="size" value="5" />
	</tiles:insertTemplate>

	<div class="d-flex justify-content-between mt-4">
		<div></div>
		<a href="/list?page=1" class="btn btn-info">Дараах</a>
	</div>


</div> --%>

<div id="showcase">
	<div class="container">
		<div id="showcase__width">
			<h1>МТ-ийн сургалтууд</h1>
			<p>Мобайл апп хөгжүүлэлт, IOS, Android, Oracle, PHP & Mysql,
				Java, Cpanel, Firewall, Сүлжээ, Active Directory, VMWare, Мэдээллийн
				аюулгүй байдал, Компьютерийн хэрэглээ, МТИШ-ын бэлтгэх сургалт...</p>
		</div>
		<div class="underline">
			<i class="fa fa-graduation-cap"></i>
		</div>
	</div>
</div>

<section>
	<div class="container">
		<div class="headline">
			<h1 class="title text-uppercase">БҮРТГЭЛ ЯВАГДАЖ БУЙ СУРГАЛТУУД</h1>
		</div>
		<div class="swiper-container">
		    <div class="swiper-wrapper">
		      <tiles:insertTemplate template="/WEB-INF/views/web/common/components/lessons.jsp" preparer="lessonPreparer">
				<tiles:putAttribute name="size" value="5"/>
			</tiles:insertTemplate>
		    </div>
		    <!-- Add Pagination -->
		    <div class="swiper-pagination"></div>
		  </div>
	</div>
</section>

<section class="intro">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 margin-bottom-10">
				<div>
					<div class="big-icon margin-bottom-10">
						<div class="icon-style margin-auto">
							<i class="fa fa-graduation-cap"></i>
						</div>
					</div>
					<div class="meta-text">
						<h3 class="text-center">Сургалтууд</h3>
						<p class="text-center">Онолын мэдлэгийг практикт тургуурлан
							олгох, дадлагажуулах, мэдлэг, чадварыг баталгаажуулан сертификат
							олгох</p>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 margin-bottom-10">
				<div>
					<div class="big-icon margin-bottom-10">
						<div class="icon-style margin-auto">
							<i class="fa fa-desktop"></i>
						</div>
					</div>
					<div class="meta-text">
						<h3 class="text-center">Сургалтын орчин</h3>
						<p class="text-center">Орчин үеийн компьютер, өндөр хурдны
							интернет, тав тухтай орчин</p>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 margin-bottom-10">
				<div>
					<div class="big-icon margin-bottom-10">
						<div class="icon-style margin-auto">
							<i class="fa fa-users"></i>
						</div>
					</div>
					<div class="meta-text">
						<h3 class="text-center">Сургагч багш нар</h3>
						<p class="text-center">Чадвартай, заах арга сайтай, албан ёсны
							сертификаттай, мэргэжлийн багш нар, салбарын шилдэг
							мэргэжилтэнүүд заана.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 4,
      spaceBetween: 20,
      loop: true,
      autoplay: {
          delay: 2500,
          disableOnInteraction: false,
        },
      freeMode: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      
      breakpoints: {
          1024: {
            slidesPerView: 4,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 2,
            spaceBetween: 10,
          },
          640: {
            slidesPerView: 1,
            spaceBetween: 5,
          },
          320: {
            slidesPerView: 1,
            spaceBetween: 5,
          }
        }
    });
  </script>
