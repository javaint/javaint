<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


	<%-- <div class="d-flex justify-content-between mt-4">
		<c:choose>
			<c:when test="${not lessons.first}">
				<a href="?page=${lessons.number-1}" class="btn btn-info">Өмнөх</a>
			</c:when>
			<c:otherwise>
				<div></div>
			</c:otherwise>
		</c:choose>

		<c:if test="${not lessons.last}">
			<a href="?page=${lessons.number+1}" class="btn btn-info">Дараах</a>
		</c:if>
	</div> --%>
<section id="showcase">
	<div class="container">
		<div id="showcase__width">
			<h1>МТ-ийн сургалтууд</h1>
			<p>Мобайл апп хөгжүүлэлт, IOS, Android, Oracle, PHP & Mysql, Java, Cpanel, Firewall, Сүлжээ, Active Directory, VMWare, Мэдээллийн аюулгүй байдал, Компьютерийн хэрэглээ, МТИШ-ын бэлтгэх сургалт...</p>
		</div>
		<div class="underline">
			<i class="fa fa-graduation-cap"></i>
		</div>
	</div>
</section>
<section>
	<div class="container">	
		<div class="headline">
			<h1 class="title text-uppercase">МАНАЙ СУРГАЛТУУД ${lesson.name}</h1>
		</div>
		<tiles:importAttribute name="lessons" ignore="true" />
		<div class="lesson">
			<c:forEach var="lesson" items="${lessons.content}">
				<div class="lesson__content">
					<c:if test="${!empty lesson.image}">
						<div class="lesson__feature">
							<img class="lesson__image" src="/${lesson.image.path}" />
						</div>
					</c:if>
					<div class="lesson__caption">
						${lesson.name}
					</div>
					<div class="lesson__description">
						${lesson.description}
					</div>
					<div class="lesson__date">
						<i class="far fa-calendar-alt"></i> <span class="date">Хугацаа:</span> <br><span class="year"><fmt:formatDate value="${lesson.start_date}" pattern="yyyy-MM-dd" /> - <fmt:formatDate value="${lesson.end_date}" pattern="yyyy-MM-dd" /></span>
					</div>
					<div class="lesson__more">
						<a href="/${lesson.id}" class="article__more-text btn_more">Дэлгэрэнгүй</a>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>  
</section>

<section class="intro">
	<div class="container">
			<div class="row">
                <div class="col-md-4 col-sm-4 margin-bottom-10">
                    <div>
                        <div class="big-icon margin-bottom-10">
                            <div class="icon-style margin-auto"><i class="fa fa-graduation-cap"></i></div>
                        </div>
                        <div class="meta-text">
                            <h3 class="text-center">Сургалтууд</h3>
                            <p class="text-center">Онолын мэдлэгийг практикт тургуурлан олгох, дадлагажуулах, мэдлэг, чадварыг баталгаажуулан сертификат олгох</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 margin-bottom-10">
                    <div>
                        <div class="big-icon margin-bottom-10">
                            <div class="icon-style margin-auto"><i class="fa fa-desktop"></i></div>
                        </div>
                        <div class="meta-text">
                            <h3 class="text-center">Сургалтын орчин</h3>
                            <p class="text-center">Орчин үеийн компьютер, өндөр хурдны интернет, тав тухтай орчин</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 margin-bottom-10">
                    <div>
                        <div class="big-icon margin-bottom-10">
                            <div class="icon-style margin-auto"><i class="fa fa-users"></i></div>
                        </div>
                        <div class="meta-text">
                            <h3 class="text-center">Сургагч багш нар</h3>
                            <p class="text-center">Чадвартай, заах арга сайтай, албан ёсны сертификаттай, мэргэжлийн багш нар, салбарын шилдэг мэргэжилтэнүүд заана.</p>
                        </div>
                    </div>
                </div>
            </div>
		</div>
</section>