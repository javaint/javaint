<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<header>
	<div class="container">
		<div id="branding">
			<a href="/"><h1>KJ Educenter</h1></a>
		</div>
		<tiles:insertTemplate template="/WEB-INF/views/web/common/components/menu.jsp" preparer="categoryPreparer"></tiles:insertTemplate>
	</div>
</header>