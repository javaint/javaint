<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:importAttribute name="lessons" ignore="true" />

<c:forEach var="lesson" items="${lessons}">
	<div class="training">
		<div class="row">
			<div class="col-md-6">
				<c:if test="${!empty lesson.image}">
					<div class="training__feature">
						<img class="training__image" src="/${lesson.image.path}" />
					</div>
				</c:if>
			</div>
			<div class="col-md-6">
				<div class="training__content">
					<a href="/${lesson.id}" class="training__title">${lesson.name}</a>
					<div class="training__date">
						<i class="far fa-calendar-alt"></i> <span class="training__date-text">Хугацаа:</span> <br><span class="training__year">${lesson.start_date} - ${lesson.end_date}</span>
					</div>
				</div>		
			</div>
		</div>		
			
			
		</div>
</c:forEach>