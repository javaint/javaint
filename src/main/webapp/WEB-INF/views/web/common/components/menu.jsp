<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:importAttribute name="categories" ignore="true" />

<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Нүүр <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Сургалтууд
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        	<a class="dropdown-item" href="/list">Бүх сургалт</a>
	        <c:forEach var="category" items="${categories}">
				<a class="dropdown-item" href="/category/${category.id}">${category.name}</a>
			</c:forEach>
        </div>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="#">Холбоо барих</a>
      </li>
    </ul>
  </div>
</nav>
