
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:importAttribute name="lessons" ignore="true" />

<c:forEach var="lesson" items="${lessons}">
	<div class="swiper-slide">
		<div class="article">		
			<c:if test="${!empty lesson.image}">
				<div class="article__feature">
					<img class="article__image" src="/${lesson.image.path}" />
				</div>
			</c:if>
			<div class="article__title">${lesson.name}</div>
			<div class="article__description">
				${lesson.description}
			</div>
			<div class="article__date">
				<i class="far fa-calendar-alt"></i> <span class="date">Хугацаа:</span> <br><span class="year">${lesson.start_date} - ${lesson.end_date}</span>
			</div>
			<div class="article__action">
				<a href="/${lesson.id}" class="article__more-text btn_more">Дэлгэрэнгүй</a>
			</div>
		</div>
	</div>
</c:forEach>